# Motion_Change_Editor


## 各ソースコードについて
### ManagementChangeMotionPrimitiveFlag
モーションを入れ替えるときに選択するボタンを管理して、モーションを選択するかどうかを管理するスクリプト<br>
https://gitlab.com/kashiwada_inalab/motion_change_editor/-/blob/main/MotionChanger/Assets/Scripts/ManagementChangeMotionPrimitiveFlag.cs
### NextMotionPrimitive
現在のシークバーから見て次(1つ右)の動作に移動させるスクリプト<br>
https://gitlab.com/kashiwada_inalab/motion_change_editor/-/blob/main/MotionChanger/Assets/Scripts/NextMotionPrimitive.cs
### PrevMotionPrimitive
現在のシークバーから見て前(1つ左)の動作に移動させるスクリプト<br>
https://gitlab.com/kashiwada_inalab/motion_change_editor/-/blob/main/MotionChanger/Assets/Scripts/PrevMotionPrimitive.cs
### Define
定数を指定するクラス<br>
https://gitlab.com/kashiwada_inalab/motion_change_editor/-/blob/main/MotionChanger/Assets/Scripts/Define.cs
### ReturnSelecedtOtherMotionPrimitiveID
どのモーションが選ばれているか返すスクリプト<br>
https://gitlab.com/kashiwada_inalab/motion_change_editor/-/blob/main/MotionChanger/Assets/Scripts/ReturnSelecedtOtherMotionPrimitiveID.cs
### ShowOtherMotionPrimitives
他の人のモーションに切り替えるボタンを表示/非表示にするスクリプト<br>
https://gitlab.com/kashiwada_inalab/motion_change_editor/-/blob/main/MotionChanger/Assets/Scripts/ShowOtherMotionPrimitives.cs
### ShowSelfMotionPrimitivesInterval
モーションプリミティブの場所を管理し、表示するスクリプト<br>
https://gitlab.com/kashiwada_inalab/motion_change_editor/-/blob/main/MotionChanger/Assets/Scripts/ShowSelfMotionPrimitivesInterval.cs
### SaveMotionSequence
変更したいモーションシーケンス、入れ替え候補のモーションプリミティブの読み込みを行い、変更、書き換えを行うスクリプト<br>
変更したモーションシーケンスを出力する<br>
https://gitlab.com/kashiwada_inalab/motion_change_editor/-/blob/main/MotionChanger/Assets/Scripts/SaveMotionSequence.cs
### ShowBignessCoefficient 
大きさの変更候補のボタンを表示/非表示の管理とどのボタンが押されたかを管理するスクリプト<br>
https://gitlab.com/kashiwada_inalab/motion_change_editor/-/blob/main/MotionChanger/Assets/Scripts/ShowBignessCoefficient.cs
### ShowSpeedCoefficient.cs 
速度の変更候補のボタンを表示/非表示の管理とどのボタンが押されたかを管理するスクリプト<br>
https://gitlab.com/kashiwada_inalab/motion_change_editor/-/blob/main/MotionChanger/Assets/Scripts/ShowSpeedCoefficient.cs

### PlayPreviewChangeSelfMotionPrimitive
本田さんの作ったAvatarManagerを元に書き換えたスクリプト<br>
Init()はspeedボタンを押すことでウィンドウが表示され、その際に呼ばれる。また、for文の中で、現在どのMotionPrimitiveを参照しているか計算している。<br>


<br>
以下本田作<br>

### PlayPreviewOtherMotionPrimitive<br>
https://gitlab.com/kashiwada_inalab/motion_change_editor/-/blob/main/MotionChanger/Assets/Scripts/PlayPreviewOtherMotionPrimitive.cs
### PlayMotionSequenceBeforeEdit<br>
https://gitlab.com/kashiwada_inalab/motion_change_editor/-/blob/main/MotionChanger/Assets/Scripts/PlayMotionSequenceBeforeEdit.cs
### ReadOtherMotionPrimitive<br>
https://gitlab.com/kashiwada_inalab/motion_change_editor/-/blob/main/MotionChanger/Assets/Scripts/ReadOtherMotionPrimitive.cs
### MoveHuman<br>
https://gitlab.com/kashiwada_inalab/motion_change_editor/-/blob/main/MotionChanger/Assets/Scripts/MoveHuman.cs