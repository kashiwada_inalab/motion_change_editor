﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;


public class RightHandRay : MonoBehaviour
{
    public GameObject pointer;
    public static string hitUIType;
    public static RaycastHit hit;
    public static string hitRootObjectName;
    public static string hitObjectName;

    void Update()
    {
        Ray ray = new Ray(this.transform.position,this.transform.forward);
        pointer.SetActive(false);
        if (Physics.Raycast(ray, out hit, 20.0f))
        {
            pointer.SetActive(true);
            pointer.transform.position = hit.point;
            hitUIType = hit.collider.tag;
            hitRootObjectName = hit.transform.root.gameObject.transform.name;
            hitObjectName = hit.transform.name;
        }
    }
}
