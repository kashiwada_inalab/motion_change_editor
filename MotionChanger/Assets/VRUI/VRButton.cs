﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using TMPro;

public class VRButton : MonoBehaviour
{
    //動作の再生、停止を管理するスクリプト
    //viveのコントローラの入力状態をいれる変数
    private SteamVR_Action_Boolean Iui = SteamVR_Actions.default_InteractUI;
    
    //動作の再生、停止の状態を決めるフラグ
    public static bool playFlag = false;
    
    //動作の再生、停止の状態を表示するための変数
    public GameObject txt;

    void Update()
    {
        //このスクリプトが設定されているオブジェクトの座標にコントローラのポインターがあるときにtrueを返す
        if (RightHandRay.hitObjectName == this.gameObject.transform.name)
        {
            //右のコントローラのトリガーボタンが押されたとき
            if (Iui.GetStateDown(SteamVR_Input_Sources.RightHand) == true)
            {
                if (playFlag == false)
                {
                    playFlag = true;
                    txt.GetComponent<TextMeshPro>().text = "Stop";
                }
                else
                {
                    playFlag = false;
                    txt.GetComponent<TextMeshPro>().text = "Start";
                }
            }
        }
    }
}
