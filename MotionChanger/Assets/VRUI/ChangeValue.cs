﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;



public class ChangeValue : MonoBehaviour
{
    public static int nowSlideValue = 0;
    public static bool changeTimeFlag = false;
    public GameObject sliderPoint;
    public GameObject startPosition;
    public GameObject endPosition;
    float sliderPosition = 0;
    float beforeValue = 0;
    float afterValue = 0;


    private SteamVR_Action_Boolean Iui = SteamVR_Actions.default_InteractUI;

    void Update()
    {
        if (this.transform.position.z < 0)
        {
            if (RightHandRay.hitRootObjectName == this.gameObject.transform.name)
            {
                if (Iui.GetState(SteamVR_Input_Sources.RightHand) == true)
                {
                    changeTimeFlag = true;
                    sliderPosition = -RightHandRay.hit.point.x + transform.root.gameObject.transform.position.x;
                }
                else
                {
                    changeTimeFlag = false;
                }

            }
            if (sliderPosition <= -startPosition.transform.position.x + transform.root.gameObject.transform.position.x)
            {
                sliderPosition = -startPosition.transform.position.x + transform.root.gameObject.transform.position.x;
            }
            if (sliderPosition >= -endPosition.transform.position.x + transform.root.gameObject.transform.position.x)
            {
                sliderPosition = -endPosition.transform.position.x + transform.root.gameObject.transform.position.x;
            }
        }

        if (Iui.GetState(SteamVR_Input_Sources.RightHand) == false||NextMotionPrimitive.nextMotionPritimiveFlag+PrevMotionPrimitive.prevMotionPritimiveFlag>=1)
        {
            sliderPosition = (float)MoveHuman.nowTime / (float)MoveHuman.endTime * 4;//sliderBar.LocalScale.x=4
            NextMotionPrimitive.nextMotionPritimiveFlag = 0;
        }
        afterValue = sliderPosition;
        if (beforeValue - afterValue != 0)
        {
            sliderPoint.transform.localPosition = new Vector3(sliderPosition, 0f, 0f);
            nowSlideValue = (int)(sliderPosition * (float)MoveHuman.endTime / 4);
        }

    }
    private void LateUpdate()
    {
        beforeValue = sliderPosition;
    }
}
