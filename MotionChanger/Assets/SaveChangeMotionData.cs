﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Valve.VR;

public class SaveChangeMotionData : MonoBehaviour
{
    private SteamVR_Action_Boolean Iui = SteamVR_Actions.default_InteractUI; 

    int motionEndPoint;// = 0; //モーションプリミティブの終わりの行を入れる変数

    //ファイル名はsize/spd+係数となっている
    string changeMotionPrimitiveNumber;      //変更したいモーションの番号を入れる変数
    string changeMotionType;        //sizeかspdかを入れる係数
    string changeMotionCoefficient; //大きさや速度の係数を入れる変数
    string[] outputChangeDatas = new string[Define.NUMBER_OF_MOVE_FILES];

    string csvPath;
    private void Start()
    {
        for(int i = 0; i < Define.NUMBER_OF_MOVE_FILES; i++)
        {
            outputChangeDatas[i] = "00";
        }
    }
    void Update()
    {
        //何番目の動作を選択しているかを計算
        for (int i = 1; i < Define.NUMBER_OF_MOVE_FILES + 1; i++)
        {
            if (ShowSelfMotionPrimitivesInterval.selfMotionPrimitiveID == "move" + i)
            {
                changeMotionPrimitiveNumber = (i - 1).ToString("00");
            }
        }

        //sizeとspdどっちを変更するのかを決定
        if (ShowBignessCoefficient.sizeChangeFlag == true && ShowSpeedCoefficient.spdChangeFlag == true)
        {
            Debug.LogError("Error:sizeChangeFlag & spdChangeFlag = true");
        }
        if (ShowBignessCoefficient.sizeChangeFlag == true)
        {
            changeMotionType = "size";
        }
        if (ShowSpeedCoefficient.spdChangeFlag == true)
        {
            changeMotionType = "spd";
        }


        if (ShowSelfMotionPrimitivesInterval.selfMotionPrimitiveStartPoint != 0)
        {
            if (ManagementChangeMotionPrimitiveFlag.changeMotionPrimitiveFlag == true)
            {
                if (ShowBignessCoefficient.sizeCoefficient != Define.VALUE_IS_NOTHING)
                {
                    changeMotionCoefficient = changeMotionType + ShowBignessCoefficient.sizeCoefficient.ToString();
                    for (int i = 0; i < Define.NUMBER_OF_SIZE_FILES; i++)
                    {
                        if (ShowBignessCoefficient.sizeCoefficient == Define.SIZE_COEFFICIENT_LIST[i])
                        {
                            outputChangeDatas[int.Parse(ShowSelfMotionPrimitivesInterval.selfMotionPrimitiveID.Replace("move", "")) - 1] = "1" + (i+1).ToString();
                            Debug.Log(ShowSelfMotionPrimitivesInterval.selfMotionPrimitiveID + "を" + Define.SIZE_COEFFICIENT_LIST[i] + "倍に変更しました。");
                        }
                    }
                    ShowBignessCoefficient.sizeCoefficient = Define.VALUE_IS_NOTHING;
                }
                else if (ShowSpeedCoefficient.speedCoefficient != Define.VALUE_IS_NOTHING)
                {
                    changeMotionCoefficient = changeMotionType + ShowSpeedCoefficient.speedCoefficient.ToString();
                    for (int i = 0; i < Define.NUMBER_OF_SPD_FILES; i++)
                    {
                        if (ShowSpeedCoefficient.speedCoefficient == Define.SPEED_COEFFICIENT_LIST[i])
                        {
                            outputChangeDatas[int.Parse(ShowSelfMotionPrimitivesInterval.selfMotionPrimitiveID.Replace("move", "")) - 1] = "2" + (i + 1).ToString();
                            Debug.Log(ShowSelfMotionPrimitivesInterval.selfMotionPrimitiveID + "を" + Define.SPEED_COEFFICIENT_LIST[i] + "倍に変更しました。");
                        }
                    }
                    ShowSpeedCoefficient.speedCoefficient = Define.VALUE_IS_NOTHING;
                }
                ManagementChangeMotionPrimitiveFlag.changeMotionPrimitiveFlag = false;
            }

        }

        if (RightHandRay.hitObjectName == this.gameObject.transform.name)
        {
            if (Iui.GetStateDown(SteamVR_Input_Sources.RightHand) == true)
            {
                if (ManagementChangeMotionPrimitiveFlag.changeMotionPrimitiveFlag == false)
                {
                    ManagementChangeMotionPrimitiveFlag.changeMotionPrimitiveFlag = true;
                    StreamWriter sw;
                    string path = Application.dataPath + "/"+Define.OUTPUT_FILE_NAME;
                    bool isAppend = false; // 上書き(false) or 追記(true)
                    sw = new StreamWriter(path, isAppend, System.Text.Encoding.GetEncoding("UTF-8"));
                    //Debug.Log("timeLengthOfMotionSequenceBeforeEdit=" + timeLengthOfMotionSequenceBeforeEdit);
                    for (int i = 0; i < Define.NUMBER_OF_MOVE_FILES; i++)
                    {
                        Debug.Log(outputChangeDatas[i]);
                    }
                    for (int i = 0; i < Define.NUMBER_OF_MOVE_FILES; i++)
                    {
                        sw.WriteLine(outputChangeDatas[i]);
                    }
                    sw.Flush();
                    sw.Close();
                }
                else
                {
                    ManagementChangeMotionPrimitiveFlag.changeMotionPrimitiveFlag = false;
                }
            }
        }
    }
}
