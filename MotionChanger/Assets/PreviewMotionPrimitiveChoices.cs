using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreviewMotionPrimitiveChoices : MonoBehaviour
{
    protected GameObject AKMotionPatternObject;
    protected AKMotionPattern AKMotionPatternScript;

    protected GameObject playPreviewChangeSelfMotionPrimitiveObject;
    protected PlayPreviewChangeSelfMotionPrimitive playPreviewChangeSelfMotionPrimitiveScritpt;
    bool activeFlag;
    void OnEnable()
    {
        activeFlag = true;
    }

    private void Update()
    {
        if (activeFlag == true)
        {
            playPreviewChangeSelfMotionPrimitiveObject = GameObject.Find("PlayPreviewChangeSelfMotionPrimitive");
            playPreviewChangeSelfMotionPrimitiveScritpt = playPreviewChangeSelfMotionPrimitiveObject.GetComponent<PlayPreviewChangeSelfMotionPrimitive>();
            playPreviewChangeSelfMotionPrimitiveScritpt.Init();
            activeFlag = false;
        }

    }
}
