﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System;
using Const;
using AKConst;
public class AKMotionManager : MonoBehaviour

{
    protected GameObject      AKMotionPatternObject;
    protected AKMotionPattern AKMotionPatternScript;

    [SerializeField] public bool isSitting = true;          // 初期姿勢が座位の場合trueを指定
    protected bool stopFlag = false;                        // 動作停止ボタンが押された場合true

    protected Quaternion[,]  motionSequenceJointQuaternions;                  // 各関節の姿勢を格納するリスト
    protected Vector3[,]     motionSequenceJointPositions;                    // 各関節の位置を格納するリスト
    protected Vector3[]      motionSequenceAvatarPositions;                   // 各フレームのアバター位置のリスト

    protected int motionSequenceLines;        // データの行数
    protected int motionSequenceColumns;      // データの列数
    protected int currentLine = 0;            // 現在参照している行
    protected float integrationTime= 0;       // フレーム時間の管理用変数

    public Matrix4x4 translationMatrix;  // 平行移動用行列

    public    Transform avatarTransform;
    public    Vector3   currentAvatarPosition;  // アタッチされたアバターの現在のフレームの位置
    protected Vector3   avatarStartPosition;    // アバターの初期位置

    protected Animator humanoidAnimator;

    protected bool firstFlag = true;//SetMotionSequenceAvatarPosition()で使用


    protected SortedDictionary<HumanBodyBones, Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>> coordinateTransformationTable; //AKのJointとUnityのBoneを対応させるテーブル
    protected List<TransformToUnity> coordinateTransformationList ;    //ボーンをTransformToUnityクラスで管理

    private void Awake()
    {
        avatarTransform = this.gameObject.GetComponent<Transform>();
        currentAvatarPosition = this.gameObject.GetComponent<Transform>().position;
        avatarStartPosition = this.gameObject.GetComponent<Transform>().position;
        humanoidAnimator = this.gameObject.GetComponent<Animator>();
    }
    public void Init()
    {
        AKMotionPatternObject = GameObject.Find("AKMotionPattern");
        AKMotionPatternScript = AKMotionPatternObject.GetComponent<AKMotionPattern>();
        coordinateTransformationTable = AKMotionPatternScript.GetcoordinateTransformationTable();
        coordinateTransformationList = AKMotionPatternScript.GetCoordinateTransformationList();
        if (firstFlag == true) {
            //avatarTransform = this.gameObject.GetComponent<Transform>();
            //currentAvatarPosition = this.gameObject.GetComponent<Transform>().position;
            //avatarStartPosition = this.gameObject.GetComponent<Transform>().position;
            //humanoidAnimator = this.gameObject.GetComponent<Animator>();
        }
    }

    public void ManagedUpdate()
    {
        //単位時間（currentFrameTime）毎に処理
        integrationTime+= Time.deltaTime;
        if (currentLine < motionSequenceLines)
        {   
            if (integrationTime> Const.CO.FRAME_TIME)
            {
                for (int i = 0; i < coordinateTransformationList .Count; i++)
                {
                    coordinateTransformationList [i].ApplyQuatToTransform(motionSequenceJointQuaternions[currentLine, i], humanoidAnimator);
                }
                currentAvatarPosition = motionSequenceAvatarPositions[currentLine];
                avatarTransform.position = currentAvatarPosition;

                currentLine++;
                integrationTime= 0;
            }
        }
        else 
        {
            if (Const.CO.IS_LOOP == false)
            {
                stopFlag = true; //ループしない場合
            }
            else
            {
                currentLine = 0;        //ループする場合
            }
        }
        GetStopFlag();
    }

    public void Reset()
    {
        currentLine = 0;
        stopFlag = false;
    }

    public bool GetStopFlag()
    {
        return this.stopFlag;
    }



    public void SetDataToAvatar(Quaternion[,] motionSequenceQuaternion, Vector3[,] motionSequencePosition)
    {
        AttachDataToAvatar(motionSequenceQuaternion, motionSequencePosition);
        SetMotionSequenceAvatarPosition();
        //SetStartPosition();

        if (firstFlag == true)
        {
            SetStartPosition();
            //初期姿勢クォータニオンのTransformへの適用
            for (int i = 0; i < coordinateTransformationList.Count; i++)
            {
                coordinateTransformationList[i].ApplyQuatToTransform(motionSequenceJointQuaternions[0, i], humanoidAnimator);
            }
            //avatarTransform.position = motionSequenceAvatarPositions[0];
            firstFlag = false;
        }
        //平行移動用行列の取得
        SetTranslateMatrix();  //※不使用
        return;
    }

    public void AttachDataToAvatar(Quaternion[,] Q, Vector3[,] V)
    {
        motionSequenceJointQuaternions = Q;
        motionSequenceJointPositions   = V;
        return;
    }
    void SetStartPosition()
    {
        // Vector3 beforePos = avatarStartPosition;
        //avatarStartPosition = new Vector3();
        float y= avatarStartPosition.y;
        if (isSitting == true)
        {
            //座位スタートの場合のゼロ位置合わせ（y座標、暫定）立位時の腰と膝の高さの差分を初期位置から引いている
            //一般化できていないので初期姿勢の形ごとにSetStartPositionを規定する必要がある。

            float yPos1 = humanoidAnimator.GetBoneTransform(HumanBodyBones.Hips).position.y;
            float yPos2 = humanoidAnimator.GetBoneTransform(HumanBodyBones.LeftLowerLeg).position.y;
            //Debug.Log("bf" + avatarStartPosition.y);
            avatarStartPosition.y -= (yPos1 - yPos2);
            //y=y-y1-y2
            //Debug.Log("af" + avatarStartPosition.y);
            
        }
        else
        {
        }
        return;
    }

    protected void SetTranslateMatrix()
    {
        Matrix4x4 m = new Matrix4x4();
        m[0, 0] = 1;
        m[1, 1] = 1;
        m[2, 2] = 1;
        m[3, 3] = 1;

        int jointInt = (int)AKConst.CO.AVATAR_ORIGIN_POINT;
        Vector3 AKcenter = motionSequenceJointPositions[0, jointInt];
        //AKcenter = TransformPosition(AKcenter);
        Vector3 UnityCenter = humanoidAnimator.GetBoneTransform(HumanBodyBones.Spine).position;

        m[0, 3] = UnityCenter.x - AKcenter.x;
        m[1, 3] = UnityCenter.y - AKcenter.y;
        m[2, 3] = UnityCenter.z - AKcenter.z;

        translationMatrix = m;
        return;
    }
    protected void SetMotionSequenceAvatarPosition()
    {
        motionSequenceLines = motionSequenceJointQuaternions.GetLength(0);
        motionSequenceAvatarPositions = new Vector3[motionSequenceLines];        
        Vector3 startPosition = motionSequenceJointPositions[0, (int)AKConst.CO.AVATAR_ORIGIN_POINT];
        for (int i = 0; i < motionSequenceLines; i++)
        {
            motionSequenceAvatarPositions[i].x = motionSequenceJointPositions[i, (int)AKConst.CO.AVATAR_ORIGIN_POINT].x;
            motionSequenceAvatarPositions[i].y = motionSequenceJointPositions[i, (int)AKConst.CO.AVATAR_ORIGIN_POINT].y;
            motionSequenceAvatarPositions[i].z = motionSequenceJointPositions[i, (int)AKConst.CO.AVATAR_ORIGIN_POINT].z;
            motionSequenceAvatarPositions[i] = motionSequenceAvatarPositions[i] - startPosition + avatarStartPosition;

        }
    }

    public Matrix4x4 GetTransformMatrix()
    {
        return translationMatrix;
    }
}

