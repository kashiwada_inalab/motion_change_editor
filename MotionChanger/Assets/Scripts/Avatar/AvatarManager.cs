﻿using System.Collections.Generic;
using UnityEngine;

public class AvatarManager : MonoBehaviour
{
    public GameObject startButtonObject;
    public StartButton startButtonScript;

    protected GameObject      AKMotionPatternObject;
    protected AKMotionPattern AKMotionPatternScript;

    protected GameObject[]    humanAvatars;
    protected List<AKMotionManager> AKMotionManagerScripts = new List<AKMotionManager>();

    //以下ループしない場合に使用。
    int stoppedAvatarCount = 0;
    int avatarNum;


    public void Init()
    {
        AKMotionPatternObject = GameObject.Find("AKMotionPattern");
        AKMotionPatternScript = AKMotionPatternObject.GetComponent<AKMotionPattern>();

        //humanAvatarsリストへのHumanタグ付加bodyの格納
        humanAvatars = GameObject.FindGameObjectsWithTag("Human");
        avatarNum = humanAvatars.Length;

        int iterator = 0;
        foreach (GameObject g in humanAvatars)
        {
            AKMotionManager m = g.GetComponent<AKMotionManager>();
            m.Init();
            m.SetDataToAvatar(AKMotionPatternScript.GetMotionSequenceQuaternion()[iterator], AKMotionPatternScript.GetMotionSequencePosition()[iterator]);
            
            AKMotionManagerScripts.Add(m);
            iterator += 1;
        }


    }
    void Update()
    {       
        if (StartButton.playFlag == true)
        {
            stoppedAvatarCount = 0;
            foreach (AKMotionManager m in AKMotionManagerScripts)
            {
                m.ManagedUpdate();
                if (m.GetStopFlag() == true) //ループしない場合の処理
                {
                    stoppedAvatarCount += 1;
                }
            }

            if (stoppedAvatarCount == avatarNum)  //ループしない場合の処理
            {
                startButtonScript.ButtonStop();
                stoppedAvatarCount = 0;
                foreach (AKMotionManager m in AKMotionManagerScripts)
                {
                    m.Reset();
                }
            }
        }
    }


}




