﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Valve.VR;

public class ShowBignessCoefficient : MonoBehaviour
{
    //大きさの変更候補のボタンを表示/非表示の管理とどのボタンが押されたかを管理するスクリプト
    private SteamVR_Action_Boolean Iui = SteamVR_Actions.default_InteractUI;
    public GameObject[] coefficient = new GameObject[Define.NUMBER_OF_SIZE_FILES];
    public static float sizeCoefficient = Define.VALUE_IS_NOTHING;
    public static bool sizeChangeFlag = false;

    public GameObject SPT;

    string[] sizeNameList = new string[Define.NUMBER_OF_SIZE_FILES];

    private void Start()
    {
        for (int i = 0; i < Define.NUMBER_OF_SIZE_FILES; i++)
        {
            sizeNameList[i] = "Bigness" + Define.SIZE_COEFFICIENT_LIST[i];
            coefficient[i].transform.name = sizeNameList[i];
            coefficient[i].transform.GetChild(0).GetComponent<TextMeshPro>().text = Define.SIZE_COEFFICIENT_LIST[i].ToString();
        }
    }

    void Update()
    {
        if (RightHandRay.hitObjectName == this.gameObject.transform.name)
        {
            if (Iui.GetStateDown(SteamVR_Input_Sources.RightHand) == true)
            {
                if (sizeChangeFlag == true)
                {
                    sizeChangeFlag = false;
                }
                else
                {
                    sizeChangeFlag = true;
                    ShowSpeedCoefficient.spdChangeFlag = false;
                }
            }
        }
        if (sizeChangeFlag == true)
        {
            SPT.SetActive(true);
            foreach (GameObject obj in coefficient)
            {
                obj.SetActive(true);
            }

            if (Iui.GetStateDown(SteamVR_Input_Sources.RightHand) == true)
            {
                for (int i = 0; i < Define.NUMBER_OF_SIZE_FILES; i++)
                {
                    if (RightHandRay.hitObjectName == sizeNameList[i])
                    {
                        coefficient[i].GetComponent<MeshRenderer>().material.color = new Color(255, 255, 255, 1.0f);
                        sizeCoefficient = sizeCoefficient = Define.SIZE_COEFFICIENT_LIST[i];
                    }
                    else
                    {
                        coefficient[i].GetComponent<MeshRenderer>().material.color = new Color(255, 255, 255, 0.5f);
                    }
                }
            }
        }
        else
        {
            SPT.SetActive(false);
            foreach (GameObject obj in coefficient)
            {
                obj.SetActive(false);
            }
            sizeCoefficient = Define.VALUE_IS_NOTHING;
        }
    }
}
