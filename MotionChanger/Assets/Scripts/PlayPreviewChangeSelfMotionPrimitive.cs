﻿using System.Collections.Generic;
using UnityEngine;


public class PlayPreviewChangeSelfMotionPrimitive : MonoBehaviour
{
    protected GameObject AKMotionPatternObject;
    protected AKMotionPattern AKMotionPatternScript;
    protected GameObject[] humanAvatars;
    protected List<AKMotionManager> AKMotionManagerScripts = new List<AKMotionManager>();


    public void Init()
    {
        AKMotionPatternObject = GameObject.Find("AKMotionPattern");
        AKMotionPatternScript = AKMotionPatternObject.GetComponent<AKMotionPattern>();
        humanAvatars = GameObject.FindGameObjectsWithTag("Preview");
        int iterator = 0;

        for (int i = 1; i < Define.NUMBER_OF_MOVE_FILES+1; i++)
        {
            if (ShowSelfMotionPrimitivesInterval.selfMotionPrimitiveID == "move" + i)
            {
                if (ShowBignessCoefficient.sizeChangeFlag == true)
                {
                    iterator = (i - 1) * 8;    //size
                    //Debug.Log(iterator);
                }
                else if(ShowSpeedCoefficient.spdChangeFlag == true){
                    iterator = (i - 1) * 8 + 4;//spd
                    //Debug.Log(iterator);
                }

            }
        }
        //Debug.Log("i" + iterator);
        AKMotionManagerScripts = new List<AKMotionManager>();

        foreach (GameObject g in humanAvatars)
        {
            AKMotionManager h = g.GetComponent<AKMotionManager>();
            h.Init();
            h.SetDataToAvatar(AKMotionPatternScript.GetMotionSequenceQuaternion()[iterator], AKMotionPatternScript.GetMotionSequencePosition()[iterator]);
            AKMotionManagerScripts.Add(h);
            iterator += 1;
        }

    }
    void Update()
    {
        //常にUpdateされている(よくない可能性がある)
        if (ShowSpeedCoefficient.spdChangeFlag)
        {
            foreach (AKMotionManager m in AKMotionManagerScripts)
            {
                m.ManagedUpdate();
            }
        }
        if (ShowBignessCoefficient.sizeChangeFlag)
        {
            foreach (AKMotionManager m in AKMotionManagerScripts)
            {
                m.ManagedUpdate();
            }
        }
    }
}




