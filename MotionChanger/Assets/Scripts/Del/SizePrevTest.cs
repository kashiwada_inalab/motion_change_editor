﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SizePrevTest : MonoBehaviour
{
    protected GameObject AKMotionPatternObject;
    protected AKMotionPattern AKMotionPatternScript;

    protected GameObject playPreviewChangeSelfMotionPrimitiveObject;
    protected PlayPreviewChangeSelfMotionPrimitive playPreviewChangeSelfMotionPrimitiveScritpt;

    //public static bool sizeActiveFlag = false;

    //public static bool sizeAFlag = false;

    bool activeFlag;
    // Start is called before the first frame update
    void OnEnable()
    {
        activeFlag = true;
        //sizeAFlag = true;
    }

    void OnDisable()
    {
        //sizeAFlag = false;
    }

    private void Update()
    {
        if (activeFlag == true)
        {
            playPreviewChangeSelfMotionPrimitiveObject = GameObject.Find("PlayPreviewChangeSelfMotionPrimitive");
            playPreviewChangeSelfMotionPrimitiveScritpt = playPreviewChangeSelfMotionPrimitiveObject.GetComponent<PlayPreviewChangeSelfMotionPrimitive>();
            playPreviewChangeSelfMotionPrimitiveScritpt.Init();
            activeFlag = false;
        }

    }
}
