using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreviewChangeSpeedMotionPrimitive : MonoBehaviour
{
    protected GameObject AKMotionPatternObject;
    protected AKMotionPattern AKMotionPatternScript;

    protected GameObject playPreviewChangeSelfMotionPrimitiveObject;
    protected PlayPreviewChangeSelfMotionPrimitive playPreviewChangeSelfMotionPrimitiveScritpt;

    bool activeFlag = false;
    void OnEnable()
    {
        activeFlag = true;
    }
    private void Update()
    {
        if (activeFlag == true)
        {
            playPreviewChangeSelfMotionPrimitiveObject = GameObject.Find("PlayPreviewChangeSelfMotionPrimitive");
            playPreviewChangeSelfMotionPrimitiveScritpt = playPreviewChangeSelfMotionPrimitiveObject.GetComponent<PlayPreviewChangeSelfMotionPrimitive>();
            playPreviewChangeSelfMotionPrimitiveScritpt.Init();
            activeFlag = false;
        }

    }
}
