﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpdPrevTest : MonoBehaviour
{
    protected GameObject AKMotionPatternObject;
    protected AKMotionPattern AKMotionPatternScript;

    protected GameObject playPreviewChangeSelfMotionPrimitiveObject;
    protected PlayPreviewChangeSelfMotionPrimitive playPreviewChangeSelfMotionPrimitiveScritpt;

    bool activeFlag = false;

    //public static bool AFlag = false;

    // Start is called before the first frame update
    void OnEnable()
    {
        activeFlag = true;
        //AFlag = true;
    }

    void OnDisable()
    {
        //AFlag = false;
    }

    private void Update()
    {
        if (activeFlag == true)
        {
            playPreviewChangeSelfMotionPrimitiveObject = GameObject.Find("PlayPreviewChangeSelfMotionPrimitive");
            playPreviewChangeSelfMotionPrimitiveScritpt = playPreviewChangeSelfMotionPrimitiveObject.GetComponent<PlayPreviewChangeSelfMotionPrimitive>();
            playPreviewChangeSelfMotionPrimitiveScritpt.Init();
            activeFlag = false;
        }

    }
}
