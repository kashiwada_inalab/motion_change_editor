﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class ShowOtherMotionPrimitives : MonoBehaviour
{
    //他の人のモーションに切り替えるボタンを表示/非表示にするスクリプト
    bool showOtherMotionPrimitiveChoiceFlag = false; //ボタンが押されたかどうかを判定するフラグ
    public GameObject[] motions=new GameObject[3];//他の人のモーションを選択するボタンのゲームオブジェクトを入れる変数
    private SteamVR_Action_Boolean Iui = SteamVR_Actions.default_InteractUI;    //viveのコントローラの入力状態をいれる変数


    void Update()
    {
        //このスクリプトが設定されているオブジェクトの座標にコントローラのポインターがあるときにtrueを返す
        if (RightHandRay.hitObjectName == this.gameObject.transform.name)
        {
            if (Iui.GetStateDown(SteamVR_Input_Sources.RightHand) == true)
            {
                if (showOtherMotionPrimitiveChoiceFlag == true)
                {
                    showOtherMotionPrimitiveChoiceFlag = false;
                }
                else
                {
                    showOtherMotionPrimitiveChoiceFlag = true;
                }
            }
        }
        if (showOtherMotionPrimitiveChoiceFlag == true)
        {
            foreach (GameObject obj in motions)
            {
                obj.SetActive(true);//設定したボタンを全て表示する
            }
        }
        else 
        {
            foreach (GameObject obj in motions)
            {
                obj.SetActive(false);//設定したボタンを全て非表示にする
            }
        }
    }
}
