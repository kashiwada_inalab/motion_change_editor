﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class ManagementChangeMotionPrimitiveFlag : MonoBehaviour
{
    //モーションを入れ替えるときに選択するボタンを管理して、モーションを選択するかどうかを管理するスクリプト
    private SteamVR_Action_Boolean Iui = SteamVR_Actions.default_InteractUI;//viveのコントローラの入力状態をいれる変数
    public static bool changeMotionPrimitiveFlag = false;//
    void Update()
    {
        if (RightHandRay.hitObjectName == this.gameObject.transform.name)
        {
            if (Iui.GetStateDown(SteamVR_Input_Sources.RightHand) == true)
            {
                if (changeMotionPrimitiveFlag == false)
                {
                    changeMotionPrimitiveFlag = true;
                }
            }
        }
    }
}
