﻿using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class MoveHuman : MonoBehaviour
{
    bool isLoop = true;

    [SerializeField] public TextAsset csvFile; // CSVファイル
    List<string[]> csvDatas = new List<string[]>(); // CSVの中身を入れるリスト;

    //int endTime; // CSVの行数
    int csvCols; // CSVの列数

    //int nowTime = 0;       //読み込んでいるCSVの行数を格納
    public static int nowTime;
    public static int endTime;

    bool stopFlag = false;

    public Transform HumanTransform;
    Quaternion[,] jointQ; //読み込んだ行のクォータニオンを格納
    Vector3[] InputPositionXYZ; //座標位置

    public static float CSVfps = 1f / 15f; //1フレームを表示する速度（秒）
    public static float frameTime = 0;     //フレーム時間の管理用変数

    public Vector3 HumanPositionXYZ; //Humanbodyの位置情報を入れるvector
    Vector3 startPositionXYZ;        //Humanbodyの初期位置
    enum XYZQuat //座標(x,y,z)およびクォータニオン（qw,qx,qy,qz）管理用のenum
    {
        x = 0,
        y = 1,
        z = 2,
        qw = 3,
        qx = 4,
        qy = 5,
        qz = 6,
    };
    int XYZQuatLen = Enum.GetNames(typeof(XYZQuat)).Length;
    enum FirstPosition //各ボーンの回転方向管理用、実際の部位に対応していないため名称は要検討
    {
        LeftArm = 0,
        RightArm = 1,
        LeftLeg = 2,
        RightLeg = 3,
    }
    //UnityのHumanBodyBonesとAzureKinectのJointId、およびクォータニオンの座標を対応させるためのDictionary.
    SortedDictionary<HumanBodyBones, Tuple<JointId, FirstPosition>> boneDict = new SortedDictionary<HumanBodyBones, Tuple<JointId, FirstPosition>>()
    {
     {HumanBodyBones.Head,              new Tuple<JointId, FirstPosition>(JointId.Head,         FirstPosition.LeftLeg  ) },
     {HumanBodyBones.Hips,              new Tuple<JointId, FirstPosition>(JointId.Pelvis,       FirstPosition.LeftLeg ) },
     {HumanBodyBones.Spine,             new Tuple<JointId, FirstPosition>(JointId.SpineNavel,   FirstPosition.LeftLeg ) },
     {HumanBodyBones.Chest,             new Tuple<JointId, FirstPosition>(JointId.SpineChest,   FirstPosition.LeftLeg ) },
     {HumanBodyBones.Neck,              new Tuple<JointId, FirstPosition>(JointId.Neck,         FirstPosition.LeftLeg  ) },

     {HumanBodyBones.LeftUpperLeg,      new Tuple<JointId, FirstPosition>(JointId.HipLeft,      FirstPosition.RightLeg  ) },
     {HumanBodyBones.LeftLowerLeg,      new Tuple<JointId, FirstPosition>(JointId.KneeLeft,     FirstPosition.RightLeg  ) },
     {HumanBodyBones.LeftFoot,          new Tuple<JointId, FirstPosition>(JointId.FootLeft,     FirstPosition.LeftLeg  ) },

     {HumanBodyBones.RightUpperLeg,     new Tuple<JointId, FirstPosition>(JointId.HipRight,    FirstPosition.LeftLeg   ) },
     {HumanBodyBones.RightLowerLeg,     new Tuple<JointId, FirstPosition>(JointId.KneeRight,     FirstPosition.LeftLeg  ) },
     {HumanBodyBones.RightFoot,         new Tuple<JointId, FirstPosition>(JointId.FootRight,    FirstPosition.RightLeg  ) },

     {HumanBodyBones.LeftShoulder,      new Tuple<JointId, FirstPosition>(JointId.ClavicleLeft, FirstPosition.LeftLeg  ) },
     {HumanBodyBones.LeftUpperArm,      new Tuple<JointId, FirstPosition>(JointId.ShoulderLeft, FirstPosition.LeftLeg ) },
     {HumanBodyBones.LeftLowerArm,      new Tuple<JointId, FirstPosition>(JointId.ElbowLeft,    FirstPosition.LeftLeg  ) },
     //{HumanBodyBones.LeftHand,          new Tuple<JointId, FirstPosition>(JointId.WristLeft,     FirstPosition.LeftLeg ) },
     //{HumanBodyBones.LeftThumbDistal,   new Tuple<JointId, FirstPosition>(JointId.ThumbLeft,    FirstPosition.LeftLeg  ) },
     //{HumanBodyBones.LeftIndexDistal,   new Tuple<JointId, FirstPosition>(JointId.HandTipLeft,    FirstPosition.LeftArm  ) },
     //{HumanBodyBones.LeftMiddleDistal,  new Tuple<JointId, FirstPosition>(JointId.HandTipLeft,  FirstPosition.LeftArm  ) },
     //{HumanBodyBones.LeftRingDistal,    new Tuple<JointId, FirstPosition>(JointId.HandTipLeft,    FirstPosition.LeftArm  ) },
     //{HumanBodyBones.LeftLittleDistal,    new Tuple<JointId, FirstPosition>(JointId.HandTipLeft,    FirstPosition.LeftArm  ) },

     {HumanBodyBones.RightShoulder,     new Tuple<JointId, FirstPosition>(JointId.ClavicleRight,FirstPosition.RightLeg ) },
     {HumanBodyBones.RightUpperArm,     new Tuple<JointId, FirstPosition>(JointId.ShoulderRight,FirstPosition.RightLeg ) },
     {HumanBodyBones.RightLowerArm,     new Tuple<JointId, FirstPosition>(JointId.ElbowRight,   FirstPosition.RightLeg ) },
     //{HumanBodyBones.RightHand,         new Tuple<JointId, FirstPosition>(JointId.WristRight,   FirstPosition.RightArm ) },
     //{HumanBodyBones.RightThumbDistal,  new Tuple<JointId, FirstPosition>(JointId.ThumbRight,   FirstPosition.RightArm ) },
     //{HumanBodyBones.RightIndexDistal,  new Tuple<JointId, FirstPosition>(JointId.HandTipRight, FirstPosition.RightArm ) },
     //{HumanBodyBones.RightMiddleDistal, new Tuple<JointId, FirstPosition>(JointId.HandTipRight, FirstPosition.RightArm ) },
     //{HumanBodyBones.RightRingDistal,   new Tuple<JointId, FirstPosition>(JointId.HandTipRight, FirstPosition.RightArm ) },
     //{HumanBodyBones.RightLittleDistal,   new Tuple<JointId, FirstPosition>(JointId.HandTipRight, FirstPosition.RightArm ) },

    };
    Animator humanoidAnimator; // 3Dモデルをインスペクタから設定する想定
    private List<TransformToUnityMH> bones;    //ボーンをTransformToUnityMHクラスで管理

    public void Init()
    {
        HumanTransform = this.gameObject.GetComponent<Transform>();
        //Debug.Log(HumanTransform);
        HumanPositionXYZ = this.gameObject.GetComponent<Transform>().position;
        startPositionXYZ = this.gameObject.GetComponent<Transform>().position;
        humanoidAnimator = this.gameObject.GetComponent<Animator>();

        SetBones();
        ReadCsv();
        //SetStartPosition();

        jointQ = new Quaternion[endTime, bones.Count * XYZQuatLen];
        InputPositionXYZ = new Vector3[endTime];

        // 座標位置合わせ
        // 最初のPervisの位置(startX,startY,startZ)からの差分として、各frameのPervisのPositionを得る
        JointId startPosJoint = JointId.Pelvis;
        float startX = -float.Parse(csvDatas[0][(int)startPosJoint * XYZQuatLen + (int)XYZQuat.x]);
        float startY = -float.Parse(csvDatas[0][(int)startPosJoint * XYZQuatLen + (int)XYZQuat.y]);
        float startZ = -float.Parse(csvDatas[0][(int)startPosJoint * XYZQuatLen + (int)XYZQuat.z]);

        for (int i = 0; i < endTime; i++)
        {
            for (int j = 0; j < bones.Count; j++)
            {
                JointId jID = bones[j].GetJointID();
                int jointInt = (int)jID;

                //座標情報入力
                ////(現在の座標)-(最初のフレームの座標)+(初期位置)
                ////csvファイルの座標はmm単位なのでm単位に変換している
                InputPositionXYZ[i].x = (-float.Parse(csvDatas[i][(int)startPosJoint * XYZQuatLen + (int)XYZQuat.x]) - startX) / 1000 + startPositionXYZ.x;
                InputPositionXYZ[i].y = (-float.Parse(csvDatas[i][(int)startPosJoint * XYZQuatLen + (int)XYZQuat.y]) - startY) / 1000 + startPositionXYZ.y;
                InputPositionXYZ[i].z = (-float.Parse(csvDatas[i][(int)startPosJoint * XYZQuatLen + (int)XYZQuat.z]) - startZ) / 1000 + startPositionXYZ.z;

                //クォータニオン入力
                jointQ[i, j].w = float.Parse(csvDatas[i][jointInt * XYZQuatLen + (int)XYZQuat.qw]);
                jointQ[i, j].x = float.Parse(csvDatas[i][jointInt * XYZQuatLen + (int)XYZQuat.qx]);
                jointQ[i, j].y = float.Parse(csvDatas[i][jointInt * XYZQuatLen + (int)XYZQuat.qy]);
                jointQ[i, j].z = float.Parse(csvDatas[i][jointInt * XYZQuatLen + (int)XYZQuat.qz]);
                //クォータニオン変換
                jointQ[i, j] = bones[j].ConvertQuatToUnity(jointQ[i, j]);
            }
        }

        //初期姿勢クォータニオンのTransformへの適用
        for (int i = 0; i < bones.Count; i++)
        {
            bones[i].ApplyQuatToTransform(jointQ[0, i]);
        }
        //初期姿勢座標のTransformへの適用
        HumanTransform.position = InputPositionXYZ[0];
    }
    public void test()
    {
        nowTime = ChangeValue.nowSlideValue;
        frameTime = nowTime * CSVfps;
    }
    public void ManagedUpdate()
    {
        //単位時間（frameTime）毎に処理
        frameTime += Time.deltaTime;
        if (nowTime < endTime)
        {
            if (frameTime > CSVfps*nowTime)
            {
                for (int i = 0; i < bones.Count; i++)
                {
                    bones[i].ApplyQuatToTransform(jointQ[nowTime, i]);
                }
                HumanPositionXYZ = InputPositionXYZ[nowTime];
                HumanTransform.position = HumanPositionXYZ;

                if (ChangeValue.changeTimeFlag == false)
                {
                    nowTime++;
                }
                else
                {
                    nowTime = ChangeValue.nowSlideValue;
                    frameTime = nowTime * CSVfps;
                }
            }
        }
        else
        {
            //stopFlag = true; //ループしない場合
            nowTime = 0;        //ループする場合
            frameTime = 0;
        }

    }

    public void Reset()
    {
        nowTime = 0;
        stopFlag = false;
    }

    public int GetRow()
    {
        return nowTime;
    }
    public bool GetFlag()
    {
        return this.stopFlag;
    }
    void SetBones()//クラスへの入力はitemを入力する？
    {
        bones = new List<TransformToUnityMH> { };
        foreach (var item in boneDict)
        {
            HumanBodyBones b = item.Key;
            (JointId j, FirstPosition fp) = item.Value;

            if (fp == FirstPosition.LeftArm)
            {
                bones.Add(new LeftArmTranformToUnityMH(b, j, humanoidAnimator));
            }
            else if (fp == FirstPosition.RightArm)
            {
                bones.Add(new RightArmTranformToUnityMH(b, j, humanoidAnimator));
            }
            else if (fp == FirstPosition.LeftLeg)
            {
                bones.Add(new LeftLegTranformToUnityMH(b, j, humanoidAnimator));
            }
            else if (fp == FirstPosition.RightLeg)
            {
                bones.Add(new RightLegTranformToUnityMH(b, j, humanoidAnimator));
            }

        }

    }
    void ReadCsv()
    {
        List<string[]> csvTemp = new List<string[]>();
        StringReader reader1 = new StringReader(csvFile.text);
        endTime = 0;

        while (reader1.Peek() > -1) // reader.Peekが0になるまで繰り返す
        {
            string line = reader1.ReadLine(); // 一行ずつ読み込み
            csvDatas.Add(line.Split(',')); // , 区切りでリストに追加
            endTime++; // 行数加算
            //endTime++;
        }
    }
    void SetStartPosition()
    {
        //座位スタートの場合のゼロ位置合わせ（y座標、暫定）立位時の腰と膝の高さの差分を初期位置から引いている
        //一般化できていないので初期姿勢の形ごとにSetStartPositionを規定する必要がある。
        float yPos1 = humanoidAnimator.GetBoneTransform(HumanBodyBones.Hips).position.y;
        float yPos2 = humanoidAnimator.GetBoneTransform(HumanBodyBones.LeftLowerLeg).position.y;
        startPositionXYZ.y -= (yPos1 - yPos2);
    }
}

//ジョイントをUnityの座標に変換するための基底クラス。各子クラスの名称は要検討。
//すべての値が0の場合Tポーズとなるように指定している
//（Unityの基本ポーズとは異なるため、MakeHuman制でないアバターを用いる場合は、それぞれ初期ポーズの変換が必要）
abstract class TransformToUnityMH
{
    protected Quaternion rotQuaternion;
    protected HumanBodyBones bone;
    protected JointId jointId;
    protected Transform humanoidJoint;

    public TransformToUnityMH(HumanBodyBones b, JointId j, Animator humanoidAnimator)
    //コンストラクタ
    {
        this.bone = b;
        this.jointId = j;
        this.humanoidJoint = humanoidAnimator.GetBoneTransform(b); ;
    }
    ~TransformToUnityMH()
    { }

    public JointId GetJointID()
    {
        return this.jointId;
    }

    public HumanBodyBones GetBone()
    {
        return this.bone;
    }

    public Transform GetTransform()
    {
        return this.humanoidJoint;
    }

    public Quaternion ConvertQuatToUnity(Quaternion Q)
    //クォータニオンを回転
    {
        return Q * this.rotQuaternion;
    }


    public Transform ApplyQuatToTransform(Quaternion Q)
    //入力されたクォータニオンをボーンのrotationにapply
    {
        this.humanoidJoint.transform.rotation = Q;
        return humanoidJoint;
    }

}


//AzureKinect用　左腕を回転させる子クラス。
class LeftArmTranformToUnityMH : TransformToUnityMH
{
    public LeftArmTranformToUnityMH(HumanBodyBones b, JointId j, Animator a) : base(b, j, a)
    {
        this.rotQuaternion = Quaternion.AngleAxis(90.0f, new Vector3(0.0f, 0.0f, 1.0f));
    }
    ~LeftArmTranformToUnityMH()
    { }


}
//AzureKinect用、右腕を回転させる子クラス。
class RightArmTranformToUnityMH : TransformToUnityMH
{
    public RightArmTranformToUnityMH(HumanBodyBones b, JointId j, Animator a) : base(b, j, a)
    {
        this.rotQuaternion = Quaternion.AngleAxis(-90.0f, new Vector3(1.0f, 0.0f, 0.0f))
                           * Quaternion.AngleAxis(-90.0f, new Vector3(0.0f, 0.0f, 1.0f));
    }
    ~RightArmTranformToUnityMH()
    { }


}

//AzureKinect用、左脚を回転させる子クラス。（体の中心を通る関節も左脚の回転を適用）
class LeftLegTranformToUnityMH : TransformToUnityMH
{
    public LeftLegTranformToUnityMH(HumanBodyBones b, JointId j, Animator a) : base(b, j, a)
    {
        this.rotQuaternion = Quaternion.AngleAxis(-90.0f, new Vector3(0.0f, 1.0f, 0.0f))
                           * Quaternion.AngleAxis(90.0f, new Vector3(1.0f, 0.0f, 0.0f));


    }
    ~LeftLegTranformToUnityMH()
    { }


}

//AzureKinect用、右腕を回転させる子クラス。
class RightLegTranformToUnityMH : TransformToUnityMH
{
    public RightLegTranformToUnityMH(HumanBodyBones b, JointId j, Animator a) : base(b, j, a)
    {
        this.rotQuaternion = Quaternion.AngleAxis(-90.0f, new Vector3(0.0f, 1.0f, 0.0f))
                           * Quaternion.AngleAxis(-90.0f, new Vector3(1.0f, 0.0f, 0.0f));
    }
    ~RightLegTranformToUnityMH()
    { }


}

// from Microsoft.Azure.Kinect.BodyTracking
public enum JointId
{
    //
    // 概要:
    //     Pelvis
    Pelvis = 0,
    //
    // 概要:
    //     Spine navel
    SpineNavel = 1,
    //
    // 概要:
    //     Spine chest
    SpineChest = 2,
    //
    // 概要:
    //     Neck
    Neck = 3,
    //
    // 概要:
    //     Left clavicle
    ClavicleLeft = 4,
    //
    // 概要:
    //     Left shoulder
    ShoulderLeft = 5,
    //
    // 概要:
    //     Left elbow
    ElbowLeft = 6,
    //
    // 概要:
    //     Left wrist
    WristLeft = 7,
    //
    // 概要:
    //     Left hand
    HandLeft = 8,
    //
    // 概要:
    //     Left hand tip
    HandTipLeft = 9,
    //
    // 概要:
    //     Left thumb
    ThumbLeft = 10,
    //
    // 概要:
    //     Right clavicle
    ClavicleRight = 11,
    //
    // 概要:
    //     Right shoulder
    ShoulderRight = 12,
    //
    // 概要:
    //     Right elbow
    ElbowRight = 13,
    //
    // 概要:
    //     Right wrist
    WristRight = 14,
    //
    // 概要:
    //     Right hand
    HandRight = 15,
    //
    // 概要:
    //     Right hand tip
    HandTipRight = 16,
    //
    // 概要:
    //     Right thumb
    ThumbRight = 17,
    //
    // 概要:
    //     Left hip
    HipLeft = 18,
    //
    // 概要:
    //     Left knee
    KneeLeft = 19,
    //
    // 概要:
    //     Left ankle
    AnkleLeft = 20,
    //
    // 概要:
    //     Left foot
    FootLeft = 21,
    //
    // 概要:
    //     Right hip
    HipRight = 22,
    //
    // 概要:
    //     Right knee
    KneeRight = 23,
    //
    // 概要:
    //     Right ankle
    AnkleRight = 24,
    //
    // 概要:
    //     Right foot
    FootRight = 25,
    //
    // 概要:
    //     Head
    Head = 26,
    //
    // 概要:
    //     Nose
    Nose = 27,
    //
    // 概要:
    //     Left eye
    EyeLeft = 28,
    //
    // 概要:
    //     Left ear
    EarLeft = 29,
    //
    // 概要:
    //     Right eye
    EyeRight = 30,
    //
    // 概要:
    //     Right ear
    EarRight = 31,
    //
    // 概要:
    //     Number of different joints defined in this enumeration.
    Count = 32
}
