﻿using UnityEngine;
using UnityEngine.UI;

public class StartButton : MonoBehaviour
{
    public GameObject startTxt;
    public static bool playFlag = false;

    public void ButtonStop()
    {
        playFlag = false;
        startTxt.GetComponent<Text>().text = "Start";
    }

    public void OnClick()
    {
        if (playFlag == false)
        {
            startTxt.GetComponent<Text>().text = "Pause";
            playFlag = true;
        }
        else
        {
            startTxt.GetComponent<Text>().text = "Start";
            playFlag = false;
        }
    }
}
