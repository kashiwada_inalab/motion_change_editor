﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Valve.VR;

public class ShowSpeedCoefficient : MonoBehaviour
{
    //速度の変更候補のボタンを表示/非表示の管理とどのボタンが押されたかを管理するスクリプト
    private SteamVR_Action_Boolean Iui = SteamVR_Actions.default_InteractUI;
    public GameObject[] coefficient = new GameObject[Define.NUMBER_OF_SPD_FILES];
    public static float speedCoefficient = Define.VALUE_IS_NOTHING;
    public static bool spdChangeFlag = false;

    public GameObject SPT;
    //public GameObject SizePT;

    string[] spdNameList=new string [Define.NUMBER_OF_SPD_FILES];
    private void Start()
    {
        for (int i = 0; i < Define.NUMBER_OF_SPD_FILES; i++)
        {
            spdNameList[i] = "Speed" + Define.SPEED_COEFFICIENT_LIST[i];
            coefficient[i].transform.name = spdNameList[i];
            coefficient[i].transform.GetChild(0).GetComponent<TextMeshPro>().text = Define.SIZE_COEFFICIENT_LIST[i].ToString();
        }
    }

    void Update()
    {
        if (RightHandRay.hitObjectName == this.gameObject.transform.name)
        {
            if (Iui.GetStateDown(SteamVR_Input_Sources.RightHand) == true)
            {
                if (spdChangeFlag == true)
                {
                    spdChangeFlag = false;
                }
                else
                {
                    spdChangeFlag = true;
                    ShowBignessCoefficient.sizeChangeFlag = false;
                }
            }
        }
        if (spdChangeFlag == true)
        {
            SPT.SetActive(true);
            foreach (GameObject obj in coefficient)
            {
                obj.SetActive(true);
            }
            if (Iui.GetStateDown(SteamVR_Input_Sources.RightHand) == true)
            {
                for (int i = 0; i < Define.NUMBER_OF_SPD_FILES; i++)
                {
                    if (RightHandRay.hitObjectName == spdNameList[i])
                    {
                        coefficient[i].GetComponent<MeshRenderer>().material.color = new Color(255, 255, 255, 1.0f);
                        speedCoefficient = speedCoefficient = Define.SPEED_COEFFICIENT_LIST[i];
                    }
                    else {
                        coefficient[i].GetComponent<MeshRenderer>().material.color = new Color(255, 255, 255, 0.5f);
                    }
                }
            }
        }
        else
        {
            SPT.SetActive(false);
            foreach (GameObject obj in coefficient)
            {
                obj.SetActive(false);
            }
            speedCoefficient = Define.VALUE_IS_NOTHING;
        }
    }
}
