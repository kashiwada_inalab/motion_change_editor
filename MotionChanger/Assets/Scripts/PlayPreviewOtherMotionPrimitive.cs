﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//他の人のモーションプリミティブを再生するスクリプト
namespace Preview
{
    public class PlayPreviewOtherMotionPrimitive : MonoBehaviour
    {
        List<ReadOtherMotionPrimitive> ReadOtherMotionPrimitiveScripts = new List<ReadOtherMotionPrimitive>();
        void Start()
        {
            GameObject HumanBody = this.gameObject;
            ReadOtherMotionPrimitive m = HumanBody.GetComponent<ReadOtherMotionPrimitive>();
            m.Init();
            ReadOtherMotionPrimitiveScripts.Add(m);
        }

        // Update is called once per frame
        void Update()
        {
            if (RightHandRay.hitObjectName == transform.root.gameObject.transform.name)
            {
                foreach (ReadOtherMotionPrimitive m in ReadOtherMotionPrimitiveScripts)
                {
                    m.ManagedUpdate();
                }
            }
        }
    }
}

