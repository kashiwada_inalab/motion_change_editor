﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Collections.ObjectModel;

namespace Const {
    public static class CO {
        public static bool IS_LOOP = true;
        public static readonly float AK_TO_UNITY_SCALE = 1f / 1000f;
        public static readonly float FRAME_TIME = 1f / 15f;
        public enum MOTION_DATA_UNIT //座標(x,y,z)およびクォータニオン（qw,qx,qy,qz）管理用のenum
        {
            x  = 0,
            y  = 1,
            z  = 2,
            qw = 3,
            qx = 4,
            qy = 5,
            qz = 6,
            Count = 7,
        };

    }

    public class QuaternionAndPosition {
        public Quaternion[,] quaternionData;
        public Vector3[,]    jointPositionData;

        public QuaternionAndPosition(Quaternion[,] Q, Vector3[,] V)
        {
            quaternionData = Q;
            jointPositionData = V;
        
        }
        ~QuaternionAndPosition()
        { }
    
    }


}