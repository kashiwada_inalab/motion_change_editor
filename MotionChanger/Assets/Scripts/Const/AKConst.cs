﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Collections.ObjectModel;

namespace AKConst {
    public static class CO {
        public static readonly Quaternion LEFT_ARM_ROTATION_QUAT  = Quaternion.AngleAxis( 90.0f, new Vector3(0.0f, 0.0f, 1.0f));
        public static readonly Quaternion RIGHT_ARM_ROTATION_QUAT = Quaternion.AngleAxis(-90.0f, new Vector3(1.0f, 0.0f, 0.0f))
                                                                  * Quaternion.AngleAxis(-90.0f, new Vector3(0.0f, 0.0f, 1.0f));
        public static readonly Quaternion LEFT_LEG_ROTATION_QUAT  = Quaternion.AngleAxis(-90.0f, new Vector3(0.0f, 1.0f, 0.0f))
                                                                  * Quaternion.AngleAxis( 90.0f, new Vector3(1.0f, 0.0f, 0.0f));
        public static readonly Quaternion RIGHT_LEG_ROTATION_QUAT = Quaternion.AngleAxis(-90.0f, new Vector3(0.0f, 1.0f, 0.0f))
                                                                  * Quaternion.AngleAxis(-90.0f, new Vector3(1.0f, 0.0f, 0.0f));
        public enum ROTATIONAL_DIRECTIONS //各ボーンの回転方向管理用、実際の部位に対応していないため名称は要検討
        {
            LeftArm = 0,
            RightArm = 1,
            LeftLeg = 2,
            RightLeg = 3,
        }
        // from Microsoft.Azure.Kinect.BodyTracking
        public enum JOINT_ID
        {
            //
            // 概要:
            //     Pelvis
            Pelvis = 0,
            //
            // 概要:
            //     Spine navel
            SpineNavel = 1,
            //
            // 概要:
            //     Spine chest
            SpineChest = 2,
            //
            // 概要:
            //     Neck
            Neck = 3,
            //
            // 概要:
            //     Left clavicle
            ClavicleLeft = 4,
            //
            // 概要:
            //     Left shoulder
            ShoulderLeft = 5,
            //
            // 概要:
            //     Left elbow
            ElbowLeft = 6,
            //
            // 概要:
            //     Left wrist
            WristLeft = 7,
            //
            // 概要:
            //     Left hand
            HandLeft = 8,
            //
            // 概要:
            //     Left hand tip
            HandTipLeft = 9,
            //
            // 概要:
            //     Left thumb
            ThumbLeft = 10,
            //
            // 概要:
            //     Right clavicle
            ClavicleRight = 11,
            //
            // 概要:
            //     Right shoulder
            ShoulderRight = 12,
            //
            // 概要:
            //     Right elbow
            ElbowRight = 13,
            //
            // 概要:
            //     Right wrist
            WristRight = 14,
            //
            // 概要:
            //     Right hand
            HandRight = 15,
            //
            // 概要:
            //     Right hand tip
            HandTipRight = 16,
            //
            // 概要:
            //     Right thumb
            ThumbRight = 17,
            //
            // 概要:
            //     Left hip
            HipLeft = 18,
            //
            // 概要:
            //     Left knee
            KneeLeft = 19,
            //
            // 概要:
            //     Left ankle
            AnkleLeft = 20,
            //
            // 概要:
            //     Left foot
            FootLeft = 21,
            //
            // 概要:
            //     Right hip
            HipRight = 22,
            //
            // 概要:
            //     Right knee
            KneeRight = 23,
            //
            // 概要:
            //     Right ankle
            AnkleRight = 24,
            //
            // 概要:
            //     Right foot
            FootRight = 25,
            //
            // 概要:
            //     Head
            Head = 26,
            //
            // 概要:
            //     Nose
            Nose = 27,
            //
            // 概要:
            //     Left eye
            EyeLeft = 28,
            //
            // 概要:
            //     Left ear
            EarLeft = 29,
            //
            // 概要:
            //     Right eye
            EyeRight = 30,
            //
            // 概要:
            //     Right ear
            EarRight = 31,
            //
            // 概要:
            //     Number of different joints defined in this enumeration.
            Count = 32
        }
        public static readonly JOINT_ID AVATAR_ORIGIN_POINT = JOINT_ID.Pelvis;
        



    }

    public abstract class TransformToUnity
    {
        //ジョイントをUnityの座標に変換するための基底クラス。各子クラスの名称は要検討。
        //各関節のクォータニオンのqx,qy,qzの値すべてが0の場合にTポーズとなるように指定している
        //（Unityの基本ポーズとは異なるため、MakeHuman制でないアバターを用いる場合は初期ポーズの変換が必要となる）


        protected Quaternion rotQuaternion;
        protected HumanBodyBones bone;
        protected AKConst.CO.JOINT_ID jointId;
        protected Transform humanoidJoint;


        protected TransformToUnity(HumanBodyBones b, AKConst.CO.JOINT_ID j)
        //コンストラクタ
        {
            this.bone = b;
            this.jointId = j;
        }
        ~TransformToUnity()
        { }

        public void SetBoneTransform(Animator humanoidAnimator)
        {

            this.humanoidJoint = humanoidAnimator.GetBoneTransform(this.bone);
        }

            public AKConst.CO.JOINT_ID GetJointId()
        {
            return this.jointId;
        }

        public HumanBodyBones GetBone()
        {
            return this.bone;
        }

        public Quaternion ConvertQuatToUnity(Quaternion Q)
        //クォータニオンを回転
        {
            return Q * this.rotQuaternion;
        }


        public void ApplyQuatToTransform(Quaternion Q, Animator humanoidAnimator)
        //入力されたクォータニオンをボーンのrotationにapply
        {
            Transform humanoidJoint = humanoidAnimator.GetBoneTransform(this.bone);

            humanoidJoint.transform.rotation = Q;
            return;
        }


    }


    //AzureKinect用　左腕を回転させる子クラス。
    class LeftArmTranformToUnity : TransformToUnity
    {

        public LeftArmTranformToUnity(HumanBodyBones b, AKConst.CO.JOINT_ID j) : base(b, j)
        {
            this.rotQuaternion = CO.LEFT_ARM_ROTATION_QUAT;
        }
        ~LeftArmTranformToUnity()
        { }


    }
    //AzureKinect用、右腕を回転させる子クラス。
    class RightArmTranformToUnity : TransformToUnity
    {
        public RightArmTranformToUnity(HumanBodyBones b, AKConst.CO.JOINT_ID j) : base(b, j)
        {
            this.rotQuaternion = CO.RIGHT_ARM_ROTATION_QUAT;
        }
        ~RightArmTranformToUnity()
        { }


    }

    //AzureKinect用、左脚を回転させる子クラス。（体の中心を通る関節も左脚の回転を適用）
    class LeftLegTranformToUnity : TransformToUnity
    {

        public LeftLegTranformToUnity(HumanBodyBones b, AKConst.CO.JOINT_ID j) : base(b, j)
        {
            this.rotQuaternion = CO.LEFT_LEG_ROTATION_QUAT;
        }
        ~LeftLegTranformToUnity()
        { }


    }

    //AzureKinect用、右脚を回転させる子クラス。
    class RightLegTranformToUnity : TransformToUnity
    {

        public RightLegTranformToUnity(HumanBodyBones b, AKConst.CO.JOINT_ID j) : base(b, j)
        {
            this.rotQuaternion = CO.RIGHT_LEG_ROTATION_QUAT;
        }
        ~RightLegTranformToUnity()
        { }


    }


}