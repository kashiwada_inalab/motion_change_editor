﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;


public class ShowSelfMotionPrimitivesInterval : MonoBehaviour
{
    //仕様変更により変わる予定
    //モーションプリミティブの場所を管理し、表示するスクリプト
    public float SMove = 87;//モーションプリミティブの始まる行数
    public float Emove = 103;//モーションプリミティブの終わる行数

    float startMotionPtimitiveLine;
    float endMotionPrimitiveLine;

    float sliderScale = 1.0f;
    private SteamVR_Action_Boolean Iui = SteamVR_Actions.default_InteractUI;//
    public static string selfMotionPrimitiveID;//何番目のモーションプリミティブが選択されているか
    public static float selfMotionPrimitiveStartPoint=0;//モーションの始まるところのポジション
    public static float selfMotionPrimitiveEndPoint=0;//ポーションの終わるところのポジション
    Collision collision;
    public static bool stayFlag=false;

    //TextAsset test; 
    //private string[] splitText1;

    private void Awake()
    {
        //TextAsset lengthOfMotionPrimitivesMemo= Resources.Load(Define.LENGTH_OF_MOTION_PRIMITIVES_MEMO_NAME, typeof(TextAsset)) as TextAsset;
        //string[] lengthOfMotionPrimitvesList= lengthOfMotionPrimitivesMemo.text.Split(char.Parse("\n"));
        //startMotionPtimitiveLine = float.Parse(lengthOfMotionPrimitvesList[(int.Parse(this.gameObject.transform.name.Replace("move", "")) - 1) * 2]);
        //endMotionPrimitiveLine   = float.Parse(lengthOfMotionPrimitvesList[(int.Parse(this.gameObject.transform.name.Replace("move", "")) - 1) * 2 + 1]);
        //test = Resources.Load("test", typeof(TextAsset)) as TextAsset;
        //splitText1 = test.text.Split(char.Parse("\n"));
        //Debug.Log(splitText1[0]);
        //Debug.Log(splitText1[int.Parse(this.gameObject.transform.name.Replace("move", ""))-1]);//i番目のmotionのスタート行数を取得(string)
        //startMotionPtimitiveLine = float.Parse(splitText1[(int.Parse(this.gameObject.transform.name.Replace("move", ""))-1) * 2]);
        //endMotionPrimitiveLine   = float.Parse(splitText1[(int.Parse(this.gameObject.transform.name.Replace("move", ""))-1) * 2 + 1]);

    }
    private void Start()
    {
        TextAsset lengthOfMotionPrimitivesMemo = Resources.Load(Define.LENGTH_OF_MOTION_PRIMITIVES_MEMO_NAME, typeof(TextAsset)) as TextAsset;
        //Debug.Log(lengthOfMotionPrimitivesMemo);
        string[] lengthOfMotionPrimitvesList = lengthOfMotionPrimitivesMemo.text.Split(char.Parse("\n"));
        startMotionPtimitiveLine = float.Parse(lengthOfMotionPrimitvesList[(int.Parse(this.gameObject.transform.name.Replace("move", "")) - 1) * 2]);
        endMotionPrimitiveLine = float.Parse(lengthOfMotionPrimitvesList[(int.Parse(this.gameObject.transform.name.Replace("move", "")) - 1) * 2 + 1]);
    }
    // Start is called before the first frame update

    void OnTriggerEnter(Collider t)
    {
    }

    void OnTriggerStay(Collider t)
    {
        selfMotionPrimitiveID = this.gameObject.name;
        //selfMotionPrimitiveStartPoint = SMove;
        //selfMotionPrimitiveEndPoint = Emove;

        selfMotionPrimitiveStartPoint = startMotionPtimitiveLine;
        selfMotionPrimitiveEndPoint   = endMotionPrimitiveLine;
        stayFlag = true;

        //Debug.Log(selfMotionPrimitiveID);
        //Debug.Log("st" + selfMotionPrimitiveStartPoint);
        //Debug.Log("end" + selfMotionPrimitiveEndPoint);
    }
    void OnTriggerExit(Collider t)
    {
        stayFlag = false;

        selfMotionPrimitiveID = "a";
        selfMotionPrimitiveStartPoint = 0;
        selfMotionPrimitiveEndPoint = 0;

        //Debug.Log(selfMotionPrimitiveID);
        //Debug.Log("st" + selfMotionPrimitiveStartPoint);
        //Debug.Log("end" + selfMotionPrimitiveEndPoint);
    }
    


    // Update is called once per frame
    void Update()
    {
        if (RightHandRay.hitObjectName == this.gameObject.transform.name)
        {
            if (Iui.GetStateDown(SteamVR_Input_Sources.RightHand) == true)
            {
                /*
                Debug.Log(true);
                selfMotionPrimitiveID = RCRayTest.objectName;
                selfMotionPrimitiveStartPoint = SMove;
                selfMotionPrimitiveEndPoint = Emove;
                Debug.Log(selfMotionPrimitiveID);
                Debug.Log("st"+selfMotionPrimitiveStartPoint);
                Debug.Log("end"+selfMotionPrimitiveEndPoint);
                */
            }
        }
        //this.gameObject.transform.localPosition = new Vector3((Emove + SMove) / 2 / (MoveHuman.endTime) * 4 * sliderScale , 0f, 0f);
        //this.gameObject.transform.localScale = new Vector3((Emove - SMove) / (MoveHuman.endTime) * 4 * sliderScale, 0.15f, 0.02f);

        this.gameObject.transform.localPosition = new Vector3((endMotionPrimitiveLine + startMotionPtimitiveLine) / 2 / (MoveHuman.endTime) * 4 * sliderScale, 0f, 0f);
        this.gameObject.transform.localScale = new Vector3((endMotionPrimitiveLine - startMotionPtimitiveLine) / (MoveHuman.endTime) * 4 * sliderScale, 0.15f, 0.02f);
    }
}
