﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;


public class PrevMotionPrimitive : MonoBehaviour
{
    //現在のシークバーから見て前(1つ左)の動作に移動させるスクリプト
    private SteamVR_Action_Boolean Iui = SteamVR_Actions.default_InteractUI;　//viveのコントローラの入力状態をいれる変数
    public GameObject[] selfMotionPrimitives = new GameObject[Define.NUMBER_OF_MOVE_FILES];//自分のモーションプリミティブを入れる配列
    public GameObject sliderPoint;//現在のスライダーの値を入れる変数
    public static int prevMotionPritimiveFlag = 0;
    int nowMove = 0;

    void Update()
    {
        if (RightHandRay.hitObjectName == this.gameObject.transform.name)
        {
            if (Iui.GetStateDown(SteamVR_Input_Sources.RightHand) == true)
            {
                for (int i = 0; i < Define.NUMBER_OF_MOVE_FILES-1; i++)
                {
                    if (selfMotionPrimitives[i].transform.position.x + selfMotionPrimitives[i].transform.localScale.x / 2 <= sliderPoint.transform.position.x)
                    {
                        nowMove++;
                    }
                }
                if (ShowSelfMotionPrimitivesInterval.stayFlag == true)
                {
                    if (nowMove >= Define.NUMBER_OF_MOVE_FILES-1)
                    {
                        nowMove = 0;
                    }
                    else
                    {
                        nowMove++;
                    }
                }
                sliderPoint.transform.position = selfMotionPrimitives[nowMove].transform.position;
                MoveHuman.nowTime = (int)(sliderPoint.transform.localPosition.x * (float)MoveHuman.endTime / 4);
                MoveHuman.frameTime = MoveHuman.nowTime * MoveHuman.CSVfps;
                prevMotionPritimiveFlag = 1;
            }
        }
        nowMove = 0;
    }
}
