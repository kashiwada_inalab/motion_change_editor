﻿using System.Collections.Generic;
using UnityEngine;

public class PlayMotionSequenceBeforeEdit : MonoBehaviour
{
    GameObject[] HumanBodies;
    List<MoveHuman> MoveHumanScripts = new List<MoveHuman>();

    void Start()
    {
        //humanBodiesリストへのHumanタグ付加bodyの格納
        HumanBodies = GameObject.FindGameObjectsWithTag("Human");
        foreach (GameObject g in HumanBodies)
        {
            MoveHuman m = g.GetComponent<MoveHuman>();
            m.Init();
            MoveHumanScripts.Add(m);
        }
    }
    void Update()
    {
        if (VRButton.playFlag == true)
        {
            foreach (MoveHuman m in MoveHumanScripts)
            {
                m.ManagedUpdate();
            }
        }
        else
        {
            foreach (MoveHuman m in MoveHumanScripts)
            {
                m.test();
            }
        }
    }
}




