﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class ReturnSelecedtOtherMotionPrimitiveID : MonoBehaviour
{
    //どのモーションが選ばれているか返すスクリプト
    private SteamVR_Action_Boolean Iui = SteamVR_Actions.default_InteractUI;
    public static int selectedOtherMotionPrimitiveID=(int)Define.VALUE_IS_NOTHING;
    public GameObject[] otherMotionPirimitives = new GameObject[Define.MOTION_NAMES.Length];

    void Update()
    {
        for (int i = 0; i < Define.MOTION_NAMES.Length; i++)
        {
            if (Iui.GetStateDown(SteamVR_Input_Sources.RightHand) == true)
            {
                if (RightHandRay.hitObjectName == Define.MOTION_NAMES[i])
                {

                    otherMotionPirimitives[i].GetComponent<MeshRenderer>().material.color = new Color(255, 255, 255, 1.0f);
                    selectedOtherMotionPrimitiveID = i;
                    //Debug.Log(ShowMoveInterval.motionName + "を" + Define.MOTION_NAMES[i] + "に変更しました。");
                }
                else
                {
                    otherMotionPirimitives[i].GetComponent<MeshRenderer>().material.color = new Color(255, 255, 255, 0.5f);
                }
            }
            else
            {
                //selectedOtherMotionPrimitiveID = (int)Define.VALUE_IS_NOTHING;
            }
        }
        //Debug.Log(selectedOtherMotionPrimitiveID);
        /*
        for (int i = 0; i < Define.MOTION_NAMES.Length; i++)
        {
            if (RCRayTest.UIName == Define.MOTION_NAMES[i])
            {
                if (Iui.GetStateDown(SteamVR_Input_Sources.RightHand) == true)
                {
                    otherMotionPirimitives[i].GetComponent<MeshRenderer>().material.color = new Color(255, 255, 255, 1.0f);
                    selectedOtherMotionPrimitiveID = i;
                    Debug.Log(ShowMoveInterval.motionName + "を" + Define.MOTION_NAMES[i] + "に変更しました。");
                }
                else
                {
                    selectedOtherMotionPrimitiveID = (int)Define.VALUE_IS_NOTHING;
                }
            }
            else
            {
                otherMotionPirimitives[i].GetComponent<MeshRenderer>().material.color = new Color(255, 255, 255, 0.5f);
            }
        }
        */

        if (selectedOtherMotionPrimitiveID != Define.VALUE_IS_NOTHING)
        {
            //string motionName = "motion" + Define.CHOICE_OTHER_MOTION[selectedOtherMotionPrimitiveID];
        }
    }
}
