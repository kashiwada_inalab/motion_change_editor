﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Valve.VR;

public class SaveMotionSequence : MonoBehaviour
{
    //仕様変更により、変更予定
    //変更したいモーションシーケンス、入れ替え候補のモーションプリミティブの読み込みを行い、変更、書き換えを行うスクリプト
    //変更したモーションシーケンスを出力する
    private SteamVR_Action_Boolean Iui = SteamVR_Actions.default_InteractUI; //viveのコントローラの入力状態をいれる変数
    [SerializeField] public TextAsset editFile; // CSVファイル

    //下記のリストや配列については以下のような構造となっている
    //一次元目:モーションシーケンスのCSVファイルの行番号(各瞬間の時刻)
    //二次元目:モーションシーケンスのCSVファイルの列番号(各関節におけるポジション(x,y,z)とクォータニオン(x,y,z,w)の値)
    List<string[]> editDatas  = new List<string[]>(); //編集したいモーションシーケンスのCSVファイルの中身を入れるリスト
    //List<string[]> tEst       = new List<string[]>(); //速度や大きさを編集したモーションプリミティブのCSVファイルの中身を入れるリスト
    List<string[]> readMotion = new List<string[]>(); //他の人のモーションプリミティブのCSVファイルの中身を入れるリスト
    float[,] edetedData; //編集後のモーションシーケンスの中身を入れる2次元配列


    //それぞれの名前に対応する行数を入れる変数
    int timeLengthOfMotionSequenceBeforeEdit;
    //int tEstRows;
    int readMotionRows;

    int motionEndPoint;// = 0; //モーションプリミティブの終わりの行を入れる変数

    //ファイル名はsize/spd+係数となっている
    string changeMotionPathNumber;      //変更したいモーションの番号を入れる変数
    string changeMotionPathType;        //sizeかspdかを入れる係数
    string changeMotionPathCoefficient; //大きさや速度の係数を入れる変数

    string csvPath;
    private void Start()
    {
        //編集したいファイルを入れる
        StringReader EReader = new StringReader(editFile.text);
        while (EReader.Peek() > -1) // reader.Peekが0になるまで繰り返す
        {
            string Eline = EReader.ReadLine(); // 一行ずつ読み込み
            editDatas.Add(Eline.Split(',')); // , 区切りでリストに追加
            timeLengthOfMotionSequenceBeforeEdit++; // 行数加算
        }

        //編集するための2重配列を初期化
        edetedData = new float[timeLengthOfMotionSequenceBeforeEdit, Define.NUMBER_OF_ALL_JOINT_ELEMENT];
        for (int i = 0; i < timeLengthOfMotionSequenceBeforeEdit; i++)
        {
            for (int j = 0; j < Define.NUMBER_OF_ALL_JOINT_ELEMENT; j++)
            {
                edetedData[i, j] = float.Parse(editDatas[i][j]);
            }
        }
    }
    void Update()
    {
        //何番目の動作を選択しているかを計算
        for (int i = 1; i < Define.NUMBER_OF_MOVE_FILES + 1; i++)
        {
            if (ShowSelfMotionPrimitivesInterval.selfMotionPrimitiveID == "move" + i)
            {
                changeMotionPathNumber = (i - 1).ToString("00");
            }
        }

        //sizeとspdどっちを変更するのかを決定
        if(ShowBignessCoefficient.sizeChangeFlag == true&& ShowSpeedCoefficient.spdChangeFlag == true)
        {
            Debug.LogError("Error:sizeChangeFlag & spdChangeFlag = true");
        }
        if (ShowBignessCoefficient.sizeChangeFlag == true)
        {
            changeMotionPathType = "size";
        }
        if (ShowSpeedCoefficient.spdChangeFlag == true)
        {
            changeMotionPathType = "spd";
        }

        
        if (ShowSelfMotionPrimitivesInterval.selfMotionPrimitiveStartPoint != 0)
        {
            if(ManagementChangeMotionPrimitiveFlag.changeMotionPrimitiveFlag == true)
            {
                if (ReturnSelecedtOtherMotionPrimitiveID.selectedOtherMotionPrimitiveID != (int)Define.VALUE_IS_NOTHING)
                {
                    csvPath = Application.dataPath + "/" + Define.MOTION_NAMES[ReturnSelecedtOtherMotionPrimitiveID.selectedOtherMotionPrimitiveID] + ".csv";
                    ReadMotionFile(csvPath);
                    motionEndPoint = readMotionRows + (int)ShowSelfMotionPrimitivesInterval.selfMotionPrimitiveStartPoint;
                    
                    for (int i = (int)ShowSelfMotionPrimitivesInterval.selfMotionPrimitiveStartPoint; i < motionEndPoint; i++)
                    {
                        for (int j = 0; j < Define.NUMBER_OF_ALL_JOINT_ELEMENT; j++)
                        {
                            edetedData[i, j] = float.Parse(readMotion[i - (int)ShowSelfMotionPrimitivesInterval.selfMotionPrimitiveStartPoint][j]);
                        }
                    }
                    Debug.Log(ShowSelfMotionPrimitivesInterval.selfMotionPrimitiveID + "を" + Define.MOTION_NAMES[ReturnSelecedtOtherMotionPrimitiveID.selectedOtherMotionPrimitiveID] + "に変更しました。");
                    ReturnSelecedtOtherMotionPrimitiveID.selectedOtherMotionPrimitiveID = (int)Define.VALUE_IS_NOTHING;
                }
                else if (ShowBignessCoefficient.sizeCoefficient != Define.VALUE_IS_NOTHING)
                {
                    changeMotionPathCoefficient = changeMotionPathType + ShowBignessCoefficient.sizeCoefficient.ToString();
                    csvPath = Application.dataPath + "/choices" + "/" + changeMotionPathNumber + "/" + changeMotionPathType + "/" + changeMotionPathCoefficient + ".csv";
                    ReadMotionFile(csvPath);
                    motionEndPoint = readMotionRows + (int)ShowSelfMotionPrimitivesInterval.selfMotionPrimitiveStartPoint;
                    for (int i = (int)ShowSelfMotionPrimitivesInterval.selfMotionPrimitiveStartPoint; i < motionEndPoint; i++)
                    {
                        for (int j = 0; j < Define.NUMBER_OF_ALL_JOINT_ELEMENT; j++)
                        {
                            edetedData[i, j] = float.Parse(readMotion[i - (int)ShowSelfMotionPrimitivesInterval.selfMotionPrimitiveStartPoint][j]);
                        }
                    }
                    for (int i = 0; i < Define.NUMBER_OF_SIZE_FILES; i++)
                    {
                        if (ShowBignessCoefficient.sizeCoefficient == Define.SIZE_COEFFICIENT_LIST[i])
                        {
                            Debug.Log(ShowSelfMotionPrimitivesInterval.selfMotionPrimitiveID + "を" + Define.SIZE_COEFFICIENT_LIST[i] + "倍に変更しました。");
                        }
                    }
                    ShowBignessCoefficient.sizeCoefficient = Define.VALUE_IS_NOTHING;
                }
                else if (ShowSpeedCoefficient.speedCoefficient != Define.VALUE_IS_NOTHING)
                {
                    changeMotionPathCoefficient = changeMotionPathType + ShowSpeedCoefficient.speedCoefficient.ToString();
                    csvPath = Application.dataPath + "/choices" + "/" + changeMotionPathNumber + "/" + changeMotionPathType + "/" + changeMotionPathCoefficient + ".csv";
                    ReadMotionFile(csvPath);
                    motionEndPoint = readMotionRows + (int)ShowSelfMotionPrimitivesInterval.selfMotionPrimitiveStartPoint;
                    for (int i = (int)ShowSelfMotionPrimitivesInterval.selfMotionPrimitiveStartPoint; i < motionEndPoint; i++)
                    {
                        for (int j = 0; j < Define.NUMBER_OF_ALL_JOINT_ELEMENT; j++)
                        {
                            edetedData[i, j] = float.Parse(readMotion[i - (int)ShowSelfMotionPrimitivesInterval.selfMotionPrimitiveStartPoint][j]);
                        }
                    }
                    for (int i = 0; i < Define.NUMBER_OF_SPD_FILES; i++)
                    {
                        if (ShowSpeedCoefficient.speedCoefficient == Define.SPEED_COEFFICIENT_LIST[i])
                        {
                            Debug.Log(ShowSelfMotionPrimitivesInterval.selfMotionPrimitiveID + "を"+ Define.SPEED_COEFFICIENT_LIST[i] + "倍に変更しました。");
                        }
                    }
                    ShowSpeedCoefficient.speedCoefficient = Define.VALUE_IS_NOTHING;
                }
                ManagementChangeMotionPrimitiveFlag.changeMotionPrimitiveFlag = false;
            }

        }

        if (RightHandRay.hitObjectName == this.gameObject.transform.name)
        {
            if (Iui.GetStateDown(SteamVR_Input_Sources.RightHand) == true)
            {
                if (ManagementChangeMotionPrimitiveFlag.changeMotionPrimitiveFlag == false)
                {
                    ManagementChangeMotionPrimitiveFlag.changeMotionPrimitiveFlag = true;
                    StreamWriter sw;
                    string path = Application.dataPath + "/TemporarilySaved.csv";
                    bool isAppend = false; // 上書き(false) or 追記(true)
                    sw = new StreamWriter(path, isAppend, System.Text.Encoding.GetEncoding("UTF-8"));
                    //Debug.Log("timeLengthOfMotionSequenceBeforeEdit=" + timeLengthOfMotionSequenceBeforeEdit);
                    for (int i = 0; i < timeLengthOfMotionSequenceBeforeEdit; i++)
                    {
                        for (int j = 0; j < Define.NUMBER_OF_ALL_JOINT_ELEMENT; j++)
                        {
                            sw.Write(edetedData[i, j]);
                            sw.Write(",");
                            if (j == Define.NUMBER_OF_ALL_JOINT_ELEMENT - 1)
                            {
                                sw.WriteLine();
                            }
                        }
                    }
                    sw.Flush();
                    sw.Close();
                }
                else
                {
                    ManagementChangeMotionPrimitiveFlag.changeMotionPrimitiveFlag = false;
                }
            }
        }
    }

    void ReadMotionFile(string path)
    {
        readMotionRows = 0;
        readMotion.Clear();
        readMotion = new List<string[]>();
        //string path = Application.dataPath + "/" + Define.MOTION_NAMES[ReturnSelecedtOtherMotionPrimitiveID.selectedOtherMotionPrimitiveID] + ".csv";
        StreamReader sr = new StreamReader(path);
        //Debug.Log(path);

        while (sr.Peek() > -1) // reader.Peekが0になるまで繰り返す
        {
            string line = sr.ReadLine(); // 一行ずつ読み込み
            readMotion.Add(line.Split(',')); // , 区切りでリストに追加
            readMotionRows++; // 行数加算
        }
    }
}
