﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Define : MonoBehaviour
{
    //定数を指定するクラス

    //係数が変わった場合に変更
    public static float[] SPEED_COEFFICIENT_LIST = { 0.75f, 0.9f, 1.1f, 1.25f };
    public static float[] SIZE_COEFFICIENT_LIST  = { 0.75f, 0.9f, 1.1f, 1.25f };

    //public static int[] CHOICE_OTHER_MOTION = { 0,1,2};
    //実装中のため変更する必要はない
    public static string[] MOTION_NAMES = {"motionA","motionB","motionC" };
    public static string[] CHOICE_OTHER_MOTION = { "A", "B", "C" };

    //ファイル数が変わった場合に変更
    public const int NUMBER_OF_SPD_FILES  = 4;
    public const int NUMBER_OF_SIZE_FILES = 4;
    public const int NUMBER_OF_MOVE_FILES = 15;
    public const int NUMBER_OF_ALL_FILES  = ( NUMBER_OF_SIZE_FILES + NUMBER_OF_SPD_FILES )* NUMBER_OF_MOVE_FILES;

    //基本変更する必要はない
    public const int NUMBER_OF_JOINT = 32;
    public const int NUMBER_OF_POSITION_ELEMENT = 3;
    public const int NUMBER_OF_QUATERNION_ELEMENT = 4;
    public const int NUMBER_OF_ALL_JOINT_ELEMENT = NUMBER_OF_JOINT * (NUMBER_OF_POSITION_ELEMENT + NUMBER_OF_QUATERNION_ELEMENT);
    public const float VALUE_IS_NOTHING = -1.0f;

    //モーションプリミティブの開始行と終了行の書いてある30行のファイルの名前(このファイルはAsset/Resources/に置く)
    public const string LENGTH_OF_MOTION_PRIMITIVES_MEMO_NAME = "id00";

    //結果を出力する際のファイル名(存在しない場合は作られる)
    //このファイルはAsset/Resources/に置く
    public const string OUTPUT_FILE_NAME = "ChangedMotionPrimitives.txt";

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
