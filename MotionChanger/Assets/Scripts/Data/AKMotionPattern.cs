﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using Const;
using AKConst;
using UnityEditor;

public class AKMotionPattern : MonoBehaviour
{
    protected List<Quaternion[,]>  wholeMotionSequenceQuaternion = new List<Quaternion[,]>();    // すべてのファイルの各関節の姿勢情報(Quaternion[,])のリスト
    protected List<Vector3[,]>     wholeMotionSequencePosition   = new List<Vector3[,]>();       // すべてのファイルの各関節の位置情報(Vector3[,])のリスト

    protected int motionSequenceFileNum;      // 読み込んだデータファイルの数
    protected int motionSequenceColumns;      // データの列数

    protected SortedDictionary<HumanBodyBones, Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>> coordinateTransformationTable = new SortedDictionary<HumanBodyBones, Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>>()
    {
     {HumanBodyBones.Head,              new Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>(AKConst.CO.JOINT_ID.Head,         AKConst.CO.ROTATIONAL_DIRECTIONS.LeftLeg ) },
     {HumanBodyBones.Hips,              new Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>(AKConst.CO.JOINT_ID.Pelvis,       AKConst.CO.ROTATIONAL_DIRECTIONS.LeftLeg ) },
     {HumanBodyBones.Spine,             new Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>(AKConst.CO.JOINT_ID.SpineNavel,   AKConst.CO.ROTATIONAL_DIRECTIONS.LeftLeg ) },
     {HumanBodyBones.Chest,             new Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>(AKConst.CO.JOINT_ID.SpineChest,   AKConst.CO.ROTATIONAL_DIRECTIONS.LeftLeg ) },
     {HumanBodyBones.Neck,              new Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>(AKConst.CO.JOINT_ID.Neck,         AKConst.CO.ROTATIONAL_DIRECTIONS.LeftLeg ) },

     {HumanBodyBones.LeftUpperLeg,      new Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>(AKConst.CO.JOINT_ID.HipLeft,      AKConst.CO.ROTATIONAL_DIRECTIONS.RightLeg ) },
     {HumanBodyBones.LeftLowerLeg,      new Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>(AKConst.CO.JOINT_ID.KneeLeft,     AKConst.CO.ROTATIONAL_DIRECTIONS.RightLeg ) },
     {HumanBodyBones.LeftFoot,          new Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>(AKConst.CO.JOINT_ID.FootLeft,     AKConst.CO.ROTATIONAL_DIRECTIONS.LeftLeg  ) },

     {HumanBodyBones.RightUpperLeg,     new Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>(AKConst.CO.JOINT_ID.HipRight,     AKConst.CO.ROTATIONAL_DIRECTIONS.LeftLeg  ) },
     {HumanBodyBones.RightLowerLeg,     new Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>(AKConst.CO.JOINT_ID.KneeRight,    AKConst.CO.ROTATIONAL_DIRECTIONS.LeftLeg  ) },
     {HumanBodyBones.RightFoot,         new Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>(AKConst.CO.JOINT_ID.FootRight,    AKConst.CO.ROTATIONAL_DIRECTIONS.RightLeg ) },

     {HumanBodyBones.LeftShoulder,      new Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>(AKConst.CO.JOINT_ID.ClavicleLeft, AKConst.CO.ROTATIONAL_DIRECTIONS.LeftLeg  ) },
     {HumanBodyBones.LeftUpperArm,      new Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>(AKConst.CO.JOINT_ID.ShoulderLeft, AKConst.CO.ROTATIONAL_DIRECTIONS.LeftLeg  ) },
     {HumanBodyBones.LeftLowerArm,      new Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>(AKConst.CO.JOINT_ID.ElbowLeft,    AKConst.CO.ROTATIONAL_DIRECTIONS.LeftLeg  ) },
     {HumanBodyBones.LeftHand,          new Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>(AKConst.CO.JOINT_ID.WristLeft,    AKConst.CO.ROTATIONAL_DIRECTIONS.LeftLeg  ) },
     //{HumanBodyBones.LeftThumbDistal,   new Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>(AKConst.CO.JOINT_ID.ThumbLeft,    AKConst.CO.ROTATIONAL_DIRECTIONS.LeftLeg  ) },
     //{HumanBodyBones.LeftIndexDistal,   new Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>(AKConst.CO.JOINT_ID.HandTipLeft,    AKConst.CO.ROTATIONAL_DIRECTIONS.LeftArm  ) },
     //{HumanBodyBones.LeftMiddleDistal,  new Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>(AKConst.CO.JOINT_ID.HandTipLeft,  AKConst.CO.ROTATIONAL_DIRECTIONS.LeftArm  ) },
     //{HumanBodyBones.LeftRingDistal,    new Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>(AKConst.CO.JOINT_ID.HandTipLeft,    AKConst.CO.ROTATIONAL_DIRECTIONS.LeftArm  ) },
     //{HumanBodyBones.LeftLittleDistal,    new Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>(AKConst.CO.JOINT_ID.HandTipLeft,    AKConst.CO.ROTATIONAL_DIRECTIONS.LeftArm  ) },

     {HumanBodyBones.RightShoulder,     new Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>(AKConst.CO.JOINT_ID.ClavicleRight,AKConst.CO.ROTATIONAL_DIRECTIONS.RightLeg ) },
     {HumanBodyBones.RightUpperArm,     new Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>(AKConst.CO.JOINT_ID.ShoulderRight,AKConst.CO.ROTATIONAL_DIRECTIONS.RightLeg ) },
     {HumanBodyBones.RightLowerArm,     new Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>(AKConst.CO.JOINT_ID.ElbowRight,   AKConst.CO.ROTATIONAL_DIRECTIONS.RightLeg ) },
     {HumanBodyBones.RightHand,         new Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>(AKConst.CO.JOINT_ID.WristRight,   AKConst.CO.ROTATIONAL_DIRECTIONS.RightLeg ) },
     //{HumanBodyBones.RightThumbDistal,  new Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>(AKConst.CO.JOINT_ID.ThumbRight,   AKConst.CO.ROTATIONAL_DIRECTIONS.RightArm ) },
     //{HumanBodyBones.RightIndexDistal,  new Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>(AKConst.CO.JOINT_ID.HandTipRight, AKConst.CO.ROTATIONAL_DIRECTIONS.RightArm ) },
     //{HumanBodyBones.RightMiddleDistal, new Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>(AKConst.CO.JOINT_ID.HandTipRight, AKConst.CO.ROTATIONAL_DIRECTIONS.RightArm ) },
     //{HumanBodyBones.RightRingDistal,   new Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>(AKConst.CO.JOINT_ID.HandTipRight, AKConst.CO.ROTATIONAL_DIRECTIONS.RightArm ) },
     //{HumanBodyBones.RightLittleDistal,   new Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>(AKConst.CO.JOINT_ID.HandTipRight, AKConst.CO.ROTATIONAL_DIRECTIONS.RightArm ) },

    };
    protected List<TransformToUnity> coordinateTransformationList;    //ボーンをTransformToUnityクラスで管理

    // Start is called before the first frame update
    public void Init()
    {
        motionSequenceColumns = (int)AKConst.CO.JOINT_ID.Count * (int)Const.CO.MOTION_DATA_UNIT.Count;
        SeCoordinateTransformationList();
        //ReadAllFiles();

    }
    public void ReadAllFiles()
    //複数のcsvファイルを読み込み、格納する
    {
        string[] files = GetAllFilePaths();

        foreach (string filePath in files)
        {
            Const.QuaternionAndPosition quaternionAndPosition = ReadCsv(filePath);
            wholeMotionSequenceQuaternion.Add(quaternionAndPosition.quaternionData);
            wholeMotionSequencePosition  .Add(quaternionAndPosition.jointPositionData );

        }
        return;
    }
    string[] GetAllFilePaths()
    {
        //読み込むすべてのファイルパスのリストを返す関数。
        string folderPath = EditorUtility.OpenFolderPanel("Select Data Folder", "Assets/Resources/", "");

        string[] files = Directory.GetFiles(folderPath, "*.csv", System.IO.SearchOption.AllDirectories);

        for (int i = 0; i < files.Length; i++)
        {
            files[i] = ToRelativePath(files[i]);
        }
        return files;

    }
    string ToRelativePath(string absolutePath)
    {
        //相対パスへの変換
        Uri sPath1 = new Uri(absolutePath);
#if UNITY_EDITOR
        string cdPath = Directory.GetCurrentDirectory();//Editor上では普通にカレントディレクトリを確認sPath1.MakeRelativeUri(sPath2);
#endif
        Uri sPath2 = new Uri(cdPath + @"\Assets\Resources\");
        string relativePath = sPath2.MakeRelativeUri(sPath1).ToString().Replace("/", @"\");
        var ext = Path.GetExtension(relativePath);
        relativePath = relativePath.Replace(ext, string.Empty);
        return relativePath;
    }


    Const.QuaternionAndPosition ReadCsv(string relativeCsvFilePath)
    {
        //ファイル名（Resources空の相対パス）を指定してcsvファイルを読み込み
        List<string[]> dataString = new List<string[]>();

        TextAsset fileTextAsset = Resources.Load(relativeCsvFilePath) as TextAsset;
        StringReader reader = new StringReader(fileTextAsset.text);
        int motionSequenceLines = 0;

        while (reader.Peek() > -1) // reader.Peekが0になるまで繰り返す
        {
            string line = reader.ReadLine(); // 一行ずつ読み込み
            dataString.Add(line.Split(',')); // , 区切りでリストに追加
            motionSequenceLines++; // 行数加算
        }

        //floatへ変換
        float[,] dataFloat = new float[motionSequenceLines, (int)AKConst.CO.JOINT_ID.Count * (int)Const.CO.MOTION_DATA_UNIT.Count];
        for (int i = 0; i < motionSequenceLines; i++)
        {
            for (int j = 0; j < ((int)Const.CO.MOTION_DATA_UNIT.Count * (int)AKConst.CO.JOINT_ID.Count); j++)
            {
                dataFloat[i,j] = float.Parse(dataString[i][j]);
            }
        }
        Const.QuaternionAndPosition quaternionAndPosition = AdjustCoodinateOfMotionSequence(dataFloat);
        
        return quaternionAndPosition;

    }


    protected void SeCoordinateTransformationList()
    {
        coordinateTransformationList = new List<TransformToUnity> { };
        foreach (var item in coordinateTransformationTable)
        {
            HumanBodyBones b = item.Key;
            (AKConst.CO.JOINT_ID j, AKConst.CO.ROTATIONAL_DIRECTIONS fp) = item.Value;

            if (fp == AKConst.CO.ROTATIONAL_DIRECTIONS.LeftArm)
            {
                coordinateTransformationList.Add(new LeftArmTranformToUnity(b, j));
            }
            else if (fp == AKConst.CO.ROTATIONAL_DIRECTIONS.RightArm)
            {
                coordinateTransformationList.Add(new RightArmTranformToUnity(b, j));
            }
            else if (fp == AKConst.CO.ROTATIONAL_DIRECTIONS.LeftLeg)
            {
                coordinateTransformationList.Add(new LeftLegTranformToUnity(b, j));
            }
            else if (fp == AKConst.CO.ROTATIONAL_DIRECTIONS.RightLeg)
            {
                coordinateTransformationList.Add(new RightLegTranformToUnity(b, j));
            }

        }
        return;

    }
    public List<Quaternion[, ]> GetMotionSequenceQuaternion()
    {
        return wholeMotionSequenceQuaternion;
    }
    public List<Vector3[,]> GetMotionSequencePosition()
    {
        return wholeMotionSequencePosition;
    }
    public SortedDictionary<HumanBodyBones, Tuple<AKConst.CO.JOINT_ID, AKConst.CO.ROTATIONAL_DIRECTIONS>> GetcoordinateTransformationTable()
    {
        return coordinateTransformationTable;
    }

    protected Const.QuaternionAndPosition AdjustCoodinateOfMotionSequence(float[,] motionSequence)
    {
        int motionSequenceLines = motionSequence.GetLength(0);

        Quaternion[,] motionSequenceQuaternion = new Quaternion[motionSequenceLines, coordinateTransformationList.Count];
        Vector3[,]    motionSequencePosition   = new Vector3   [motionSequenceLines, coordinateTransformationList.Count];

        for (int i = 0; i < motionSequenceLines; i++)
        {
            for (int j = 0; j < coordinateTransformationList.Count; j++)
            {
                AKConst.CO.JOINT_ID jID = coordinateTransformationList [j].GetJointId();
                int jointInt = (int)jID;


                Quaternion tempQuaternion;
                Vector3    tempVector;

                //座標情報入力

                tempVector.x = motionSequence[i, (int)AKConst.CO.AVATAR_ORIGIN_POINT * (int)Const.CO.MOTION_DATA_UNIT.Count + (int)Const.CO.MOTION_DATA_UNIT.x];
                tempVector.y = motionSequence[i, (int)AKConst.CO.AVATAR_ORIGIN_POINT * (int)Const.CO.MOTION_DATA_UNIT.Count + (int)Const.CO.MOTION_DATA_UNIT.y];
                tempVector.z = motionSequence[i, (int)AKConst.CO.AVATAR_ORIGIN_POINT * (int)Const.CO.MOTION_DATA_UNIT.Count + (int)Const.CO.MOTION_DATA_UNIT.z];

                tempVector = TransformPosition(tempVector) * Const.CO.AK_TO_UNITY_SCALE;

                //クォータニオン入力

                tempQuaternion.w = motionSequence[i, jointInt * (int)Const.CO.MOTION_DATA_UNIT.Count + (int)Const.CO.MOTION_DATA_UNIT.qw];
                tempQuaternion.x = motionSequence[i, jointInt * (int)Const.CO.MOTION_DATA_UNIT.Count + (int)Const.CO.MOTION_DATA_UNIT.qx];
                tempQuaternion.y = motionSequence[i, jointInt * (int)Const.CO.MOTION_DATA_UNIT.Count + (int)Const.CO.MOTION_DATA_UNIT.qy];
                tempQuaternion.z = motionSequence[i, jointInt * (int)Const.CO.MOTION_DATA_UNIT.Count + (int)Const.CO.MOTION_DATA_UNIT.qz];
                //クォータニオン変換
                tempQuaternion = coordinateTransformationList [j].ConvertQuatToUnity(tempQuaternion);

                motionSequenceQuaternion[i, j] = tempQuaternion;
                motionSequencePosition[i, j] = tempVector;




            }
        }
        Const.QuaternionAndPosition quaternionAndPostion = new Const.QuaternionAndPosition(motionSequenceQuaternion, motionSequencePosition);

        return quaternionAndPostion;
    }

    public Vector3 TransformPosition(Vector3 v)
    {
        //AK>Unity
        return -v;

    }


    public List<TransformToUnity> GetCoordinateTransformationList()
    {
        return coordinateTransformationList;
    }
}

